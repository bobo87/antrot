/*
 * File:   TLEEditForm.h
 * Author: peter
 *
 * Created on October 24, 2012, 8:51 PM
 */

#ifndef _TLEEDITFORM_H
#define	_TLEEDITFORM_H

#include "TLE.h"

#include "TLEUpdateSource.h"
#include "ui_TLEEditForm.h"



class TLEEditForm : public QDialog, public TLEUpdateSource {
	Q_OBJECT
public:
	TLEEditForm(const TLE& tle = TLE());
	virtual ~TLEEditForm();

	inline void update() {
		TLEUpdateSource::update(tle);
	}

	inline TLE getTLE() const {
		return tle;
	}

private:
	Ui::TLEEditForm widget;

	TLE tle;

public slots:
	void textChanged();
	inline void accept() {
		update();
		QDialog::accept();
	}
};

#endif	/* _TLEEDITFORM_H */
