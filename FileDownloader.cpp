#include "FileDownloader.h"



using namespace std;



FileDownloader::FileDownloader(const std::string& url, const std::string& destFile) :
	url(url),
	file(0){
	curl = curl_easy_init();
	if(!curl)
		throw DownloadError("can't create curl instance");

	outputStream.fileString = &data;
	if(destFile!=""){
		file              = new ofstream(destFile.c_str());
		outputStream.file = file;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &outputStream);
}



FileDownloader::~FileDownloader(){
	if(file)
		delete file;
	if(curl)
		curl_easy_cleanup(curl);
}



size_t FileDownloader::writeCallback(void *data, size_t size, size_t nmemb, OutputStream *stream){
	if(stream->file)
		stream->file->write((char*)data, size*nmemb);
	if(stream->fileString)
		stream->fileString->append((char*)data, size*nmemb);
	return size*nmemb;
}



std::string FileDownloader::download(){
	CURLcode res = curl_easy_perform(curl);
	if(res!=CURLE_OK)
		throw DownloadError(curl_easy_strerror(res));
	return data;
}
