#include "GridMapLayer.h"



void GridMapLayer::render(QPainter& painter){
	painter.setPen(QColor(0,0,0,127));

	const int   LAT_LINES = 17;
	const float LAT_STEP  = getMap()->height()/(LAT_LINES+1.0f);
	for(int i=0; i<LAT_LINES; i++){
		const int Y = (int)(LAT_STEP*(i+1));
		painter.drawLine(0, Y, getMap()->width(), Y);
	}

	const int   LON_LINES = 35;
	const float LON_STEP  = getMap()->width()/(LON_LINES+1.0f);
	for(int i=0; i<LON_LINES; i++){
		const int X = (int)(LON_STEP*(i+1));
		painter.drawLine(X, 0, X, getMap()->height());
	}

}
