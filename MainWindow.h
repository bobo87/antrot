/*
 * File:   MainWindow.h
 * Author: peter
 *
 * Created on October 12, 2012, 10:33 PM
 */

#ifndef _MAINWINDOW_H
#define	_MAINWINDOW_H

#include "ui_MainWindow.h"

#include "Tracker.h"
#include "Satellite.h"
#include "misc.h"



class SatellitePredictor;
class Satellite;



class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow();
	virtual ~MainWindow();

private:
	void processMenu();
	void satelliteAdd(TLE& tle);

	Ui::MainWindow widget;

	SatellitesVector satellites;

	Tracker *tracker;

public slots:
	void mapRedrawTimer();
	void tleChanged(int index);

	void connectButton();

	void tleAdd();
	void tleEdit();
	void tleRemove();
	void tleUpdate();

	void satelliteAddNew();
	void satelliteAddExisting();
	void satelliteEdit();
	void satelliteRemove();
};

#endif	/* _MAINWINDOW_H */
