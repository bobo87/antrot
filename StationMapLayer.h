/*
 * File:   StationMapLayer.h
 * Author: peter
 *
 * Created on October 13, 2012, 6:59 PM
 */

#ifndef STATIONMAPLAYER_H
#define	STATIONMAPLAYER_H



#include "MapLayer.h"
#include "GroundStationPosition.h"

#include <QPixmap>


class StationMapLayer : public MapLayer{
public:
	StationMapLayer(WorldMap *map) :
		MapLayer(map), station(0.0, 0.0, 0.0), icon("./data/gfx/dish16.png") { }

	void render(QPainter& painter);

	inline void setStationPosition(GroundStationPosition station) { this->station = station; }
	
private:
	GroundStationPosition station;
	QPixmap icon;
};


#endif	/* STATIONMAPLAYER_H */

