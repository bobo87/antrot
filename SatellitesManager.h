/*
 * File:   SatellitesManager.h
 * Author: peter
 *
 * Created on January 26, 2013, 6:31 PM
 */

#ifndef SATELLITESMANAGER_H
#define	SATELLITESMANAGER_H



#include "Satellite.h"
#include "Singleton.h"
#include "misc.h"

#include <stdexcept>
#include <algorithm>



class SatellitesManager : public Singleton<SatellitesManager> {
public:

	const static std::string SATELLITES_FILE;

	inline void add(Satellite* satellite) {
		if(std::find(satellites.begin(), satellites.end(), satellite) == satellites.end())
			satellites.push_back(satellite);
	}

	inline void remove(Satellite* satellite) {
		SatellitesVector::iterator it = std::find(satellites.begin(), satellites.end(), satellite);
		if(it != satellites.end())
			satellites.erase(it);
	}

	inline std::size_t size() const {
		return satellites.size();
	}

	inline bool exist(const std::string& name){
		for(SatellitesVector::const_iterator it=satellites.begin(); it!=satellites.end(); it++)
			if((*it)->getTLE().getName() == name)
				return true;
		return false;
	}

	inline Satellite* getSatellite(std::size_t index){
		return satellites[index];
	}

	inline void setSatellite(std::size_t index, Satellite *satellite){
		satellites[index] = satellite;
	}

	inline int satelliteToIndex(Satellite* satellite){
		SatellitesVector::iterator found = std::find(satellites.begin(), satellites.end(), satellite);
		return found == satellites.end() ? -1 : std::distance(satellites.begin(), found);
	}

	inline int satelliteToIndex(const std::string& name) {
		for(std::size_t i=0; i<satellites.size(); i++)
			if(name == satellites[i]->getTLE().getName())
				return i;
		return -1;
	}

	inline StringVector getSatelliteNames() const {
		StringVector ret;

		for(SatellitesVector::const_iterator it=satellites.begin(); it!=satellites.end(); it++)
			ret.push_back((*it)->getTLE().getName());

		return ret;
	}

	void save();
	void load();

	class LoadingError : public std::runtime_error{
	public:
		LoadingError(const std::string& error) :
			std::runtime_error(error.c_str()) { }
	};

private:
	friend class Singleton<SatellitesManager>;
	SatellitesManager() { }

	SatellitesVector satellites;
};



#endif	/* SATELLITESMANAGER_H */
