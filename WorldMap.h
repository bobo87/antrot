/*
 * File:   map.h
 * Author: peter
 *
 * Created on October 12, 2012, 11:17 PM
 */

#ifndef WORLDMAP_H
#define	WORLDMAP_H



#include "Settings.h"
#include "TimeBase.h"
#include "SatellitePosition.h"
#include "GroundStationPosition.h"
#include "GroundStationPositionListener.h"
#include "misc.h"

#include <QWidget>



class SatelliteCoverageMapLayer;
class DaylightMapLayer;
class TrajectoryMapLayer;
class SatellitesMapLayer;
class StationMapLayer;
class GridMapLayer;




class WorldMap : public QWidget, public GroundStationPositionListener{
	Q_OBJECT
public:
	WorldMap(QWidget* widget);
	virtual ~WorldMap();

	void setGroundStationPosition(GroundStationPosition position);
	void setSatellitesVector(SatellitesVector *satellites);

	inline void onGroundStationPositionChanged(GroundStationPosition newPosition){
		setGroundStationPosition(newPosition);
	}
	
protected:
	void paintEvent(QPaintEvent *event);
	void resizeEvent(QResizeEvent *event);

private:

	QPixmap *mapDay;

	QPixmap *mapDayResized, *mapNightResized;


	GridMapLayer *gridLayer;
	StationMapLayer *stationLayer;
	SatellitesMapLayer *satellitesLayer;
	TrajectoryMapLayer *trajectoryLayer;
	DaylightMapLayer *daylightLayer;
	SatelliteCoverageMapLayer *satelliteCoverageLayer;

	SatellitesVector *satellites;

	UnixTime lastSavedTime;

	Settings *settings;
};


#endif	/* MAP_H */
