/*
 * File:   SatelliteEditor.h
 * Author: peter
 *
 * Created on January 13, 2013, 10:29 PM
 */

#ifndef _SATELLITEEDITOR_H
#define	_SATELLITEEDITOR_H

#include "ui_SatelliteEditor.h"

#include "Satellite.h"



class SatelliteEditor : public QDialog {
	Q_OBJECT
public:
	SatelliteEditor(Satellite *satellite);
	virtual ~SatelliteEditor();

private:
	Ui::SatelliteEditor widget;
	Satellite *satellite;

private slots:
	void accepted();
};

#endif	/* _SATELLITEEDITOR_H */
