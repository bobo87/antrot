/*
 * File:   TrajecoryMapLayer.h
 * Author: peter
 *
 * Created on October 14, 2012, 12:58 AM
 */

#ifndef TRAJECORYMAPLAYER_H
#define	TRAJECORYMAPLAYER_H



#include "MapLayer.h"
#include "SatellitePosition.h"
#include "misc.h"

#include <QPainter>



class TrajectoryMapLayer : public MapLayer{
public:
	TrajectoryMapLayer(WorldMap *map) :
		MapLayer(map), satellites(0) { }

	void render(QPainter& painter);

	inline void setSatellitesVector(SatellitesVector *satellites) { this->satellites = satellites; }

private:
	SatellitesVector* satellites;
};



#endif	/* TRAJECORYMAPLAYER_H */

