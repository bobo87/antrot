/*
 * File:   TLEUpdateForm.h
 * Author: peter
 *
 * Created on July 14, 2013, 11:26 AM
 */

#ifndef _TLEUPDATEFORM_H
#define	_TLEUPDATEFORM_H

#include "ui_TLEUpdateForm.h"

#include <string>


class TLEUpdateForm : public QDialog {
	Q_OBJECT
public:
	TLEUpdateForm();
	virtual ~TLEUpdateForm();

public slots:
	void sourceSelected(int row);
	void plusButton();
	void minusButton();
	void sortButton();
	void accepted();

private:
	void loadSources();
	void saveSources();

	Ui::TLEUpdateForm widget;

	const static std::string SOURCES_FILE;
};



#endif	/* _TLEUPDATEFORM_H */
