#include "Trajectory.h"



void Trajectory::recordPoint(SatellitePosition position){
	trajectory.push_back(position);
	if(trajectory.size()>maxPoints)
		trajectory.erase(trajectory.begin(), trajectory.begin() + (trajectory.size()-maxPoints) + 1);
}
