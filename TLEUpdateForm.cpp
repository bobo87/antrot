/*
 * File:   TLEUpdateForm.cpp
 * Author: peter
 *
 * Created on July 14, 2013, 11:26 AM
 */

#include "TLEUpdateForm.h"

#include "misc.h"

#include <fstream>



using namespace std;



const std::string TLEUpdateForm::SOURCES_FILE = "./data/tle/sources.txt";



TLEUpdateForm::TLEUpdateForm(){
	widget.setupUi(this);
	loadSources();

	connect(widget.sourcesList, SIGNAL(currentRowChanged(int)), this, SLOT(sourceSelected(int)));
	connect(widget.plusButton, SIGNAL(pressed()), this, SLOT(plusButton()));
	connect(widget.minusButton, SIGNAL(pressed()), this, SLOT(minusButton()));
	connect(widget.sortButton, SIGNAL(pressed()), this, SLOT(sortButton()));

	connect(this, SIGNAL(accepted()), this, SLOT(accepted()));
}



TLEUpdateForm::~TLEUpdateForm(){
}



void TLEUpdateForm::loadSources(){
	ifstream file(SOURCES_FILE.c_str());
	string line;

	if(!file)
		return;

	widget.sourcesList->clear();

	while(!file.eof()){
		getline(file, line);
		trim(line);
		if(line == "")
			continue;
		widget.sourcesList->addItem(QString::fromStdString(line));
	}

	widget.updateProgress->setMaximum(widget.sourcesList->count());
	widget.updateProgress->setValue(0);
}



void TLEUpdateForm::saveSources(){
	ofstream file(SOURCES_FILE.c_str());

	if(!file)
		return;

	for(int i=0; i<widget.sourcesList->count(); i++)
		file << widget.sourcesList->item(i)->text().toStdString() << endl;
}



void TLEUpdateForm::sourceSelected(int row){
	widget.sourcesEdit->setText(widget.sourcesList->item(row)->text());
}



void TLEUpdateForm::plusButton(){
	widget.sourcesList->addItem(widget.sourcesEdit->text());
	widget.updateProgress->setMaximum(widget.sourcesList->count());
}



void TLEUpdateForm::minusButton(){
	if(widget.sourcesList->selectedItems().size() != 1)
		return;
	delete widget.sourcesList->item(widget.sourcesList->currentRow());
	widget.updateProgress->setMaximum(widget.sourcesList->count());
}



void TLEUpdateForm::sortButton(){
	widget.sourcesList->sortItems();
}



void TLEUpdateForm::accepted(){
	saveSources();
}
