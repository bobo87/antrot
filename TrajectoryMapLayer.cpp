#include "TrajectoryMapLayer.h"

#include "SatellitesManager.h"
#include "Satellite.h"



void TrajectoryMapLayer::render(QPainter& painter){
	if(!satellites)
		return;

	//TODO namiesto prekreslovania stale celych trajektorii pouzit buffer a stale vykreslit len posledne body (podobne, ako pre mriezku)
	painter.setPen(QColor(255,0,0));
//	for(size_t i=0; i<satellites->size(); i++){
//		Satellite* satellite = satellites->at(i);
	for(size_t i=0; i<SatellitesManager::getInstance()->size(); i++){
		Satellite* satellite = SatellitesManager::getInstance()->getSatellite(i);

		if(satellite->getTrajectory().size() < 2)
			continue;

		for(int j=0; j<satellite->getTrajectory().size(); j++){
			if(!j)
				continue;

			const int
				X1 = longitudeToPixel(satellite->getTrajectory()[j-1].getLongitude()),
				X2 = longitudeToPixel(satellite->getTrajectory()[j].getLongitude());

			if(abs(X1-X2) >= getMap()->width()/10)
				continue;

			painter.drawLine(
					X1,
					latitudeToPixel(satellite->getTrajectory()[j-1].getLatitude()),
					X2,
					latitudeToPixel(satellite->getTrajectory()[j].getLatitude())
					);
		}
	}
}



