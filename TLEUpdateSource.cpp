#include "TLEUpdateSource.h"



void TLEUpdateSource::update(const TLE& tle){
	for(Listeners::iterator it=listeners.begin(); it!=listeners.end(); it++)
		(*it)->update(tle);
}
