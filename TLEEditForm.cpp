/*
 * File:   TLEEditForm.cpp
 * Author: peter
 *
 * Created on October 24, 2012, 8:51 PM
 */

#include "TLEEditForm.h"

#include "misc.h"

#include <QPushButton>
#include <iostream>
#include <string>



using namespace std;



TLEEditForm::TLEEditForm(const TLE& tle) : tle(tle) {
	widget.setupUi(this);

	connect(widget.nameEdit, SIGNAL(textChanged(QString)), this, SLOT(textChanged(void)));
	connect(widget.line1Edit, SIGNAL(textChanged(QString)), this, SLOT(textChanged(void)));
	connect(widget.line2Edit, SIGNAL(textChanged(QString)), this, SLOT(textChanged(void)));
	widget.nameEdit->setText(tle.getLine(0).c_str());
	widget.line1Edit->setText(tle.getLine(1).c_str());
	widget.line2Edit->setText(tle.getLine(2).c_str());
	textChanged();
}



TLEEditForm::~TLEEditForm(){
}



void TLEEditForm::textChanged(){
	tle = TLE(
		widget.nameEdit->text().toAscii().constData(),
		widget.line1Edit->text().toAscii().constData(),
		widget.line2Edit->text().toAscii().constData());
	if(tle.isValid()){
		widget.validityLabel->setText(tr("TLE is <font color='green'>VALID</font>"));
		widget.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
	}else{
		widget.validityLabel->setText(tr("TLE is <font color='red'>INVALID</font>"));
		widget.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
	}
}
