/*
 * File:   TLEManager.h
 * Author: peter
 *
 * Created on October 20, 2012, 12:18 PM
 */

#ifndef TLEMANAGER_H
#define	TLEMANAGER_H



#include "TLEUpdateSource.h"
#include "Singleton.h"
#include "misc.h"
#include "pugixml.hpp"

#include <map>



class TLEManager : public Singleton<TLEManager>, public TLEUpdateListener{
public:
	const static std::string TLE_FILE;

	void update(const TLE& tle);
	void update(TLEUpdateSource *source);

	StringVector getSatelliteNames() const;

	inline bool exist(const std::string& name) const{
		return data.find(name) != data.end();
	}

	inline TLE getTLE(const std::string& name) {
		return data[name];
	}

	void remove(const std::string& name){
		TLEMap::iterator item = data.find(name);
		if(item != data.end())
			data.erase(item);
	}

	inline void clear(){
		data.clear();
	}

	void save();
	void load();

	inline std::size_t getCount() const {
		return data.size();
	}

	class LoadingError : public std::runtime_error{
	public:
		LoadingError(const std::string& error) :
			std::runtime_error(error.c_str()) { }
	};

private:
	typedef std::map<std::string, TLE> TLEMap;
	TLEMap data;

	friend class Singleton<TLEManager>;
	TLEManager() { }
	~TLEManager() { }

	void addNode(pugi::xml_node root, const TLE& tle);
};



#endif	/* TLEMANAGER_H */
