/*
 * File:   main.cpp
 * Author: peter
 *
 * Created on October 10, 2012, 7:55 PM
 */


// ptypes 2.1.1
// Eigen 3.1.1

#include "MainWindow.h"
#include "TLEEditForm.h"

#include <QApplication>
#include <iostream>
#include <ctime>
#include <string>



using namespace std;



//std::string unixToTime(std::time_t unixTime){
//	return asctime(localtime(&unixTime));
//}


int main(int argc, char *argv[]) {
	// initialize resources, if needed
	// Q_INIT_RESOURCE(resfile);

	QApplication app(argc, argv);

	MainWindow mainWindow;
	mainWindow.show();
	//TLEEditForm tleEditor;
	//tleEditor.show();

	return app.exec();
}
