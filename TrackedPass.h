/*
 * File:   TrackedPass.h
 * Author: peter
 *
 * Created on January 5, 2013, 2:20 PM
 */

#ifndef TRACKEDPASS_H
#define	TRACKEDPASS_H



#include "Satellite.h"
#include "TimeSlot.h"



class TrackedPass{
public:
	TrackedPass(TimeSlot timeSlot = TimeSlot(), Satellite* satellite = 0) : time(timeSlot), satellite(satellite) { }

	TimeSlot getTime();
	Satellite *getSatellite();

	inline bool isNull() const { return time.isNull(); }
	inline bool operator!() const { return isNull(); }

private:
	TimeSlot time;
	Satellite *satellite;
};



#endif	/* TRACKEDPASS_H */
