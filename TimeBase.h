/*
 * File:   TimeBase.h
 * Author: peter
 *
 * Created on December 3, 2012, 9:25 PM
 */

#ifndef TIMEBASE_H
#define	TIMEBASE_H



#include "Singleton.h"

#include <string>
#include <ctime>
#include <stdint.h>



//typedef uint64_t UnixTime;
typedef time_t UnixTime;



class TimeBase : public Singleton<TimeBase>{
public:
	UnixTime getTime();
	UnixTime getSystemTime();

	inline void setDelta(long delta) {
		this->delta = delta;
	}

	inline long getDelta() const {
		return delta;
	}

private:
	friend class Singleton<TimeBase>;
	TimeBase() : delta(0) { }

	long delta;
};



std::string timeToString(UnixTime unixTime);



#endif	/* TIMEBASE_H */
