#include "SatelliteCoverageMapLayer.h"

#include "SatellitesManager.h"
#include "Satellite.h"

#include <eigen3/Eigen/Eigen>
#include <QPixmap>
#include <iostream>
#include <cmath>



////  Globals which should be set before calling this function:
////
////  int    polySides  =  how many corners the polygon has
////  float  polyX[]    =  horizontal coordinates of corners
////  float  polyY[]    =  vertical coordinates of corners
////  float  x, y       =  point to be tested
////
////  (Globals are used in this example for purposes of speed.  Change as
////  desired.)
////
////  The function will return YES if the point x,y is inside the polygon, or
////  NO if it is not.  If the point is exactly on the edge of the polygon,
////  then the function may return YES or NO.
////
////  Note that division by zero is avoided because the division is protected
////  by the "if" clause which surrounds it.
//
//bool pointInPolygon() {
//
//  int      i, j=polySides-1 ;
//  boolean  oddNodes=NO      ;
//
//  for (i=0; i<polySides; i++) {
//    if ((polyY[i]< y && polyY[j]>=y
//    ||   polyY[j]< y && polyY[i]>=y)
//    &&  (polyX[i]<=x || polyX[j]<=x)) {
//      oddNodes^=(polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x); }
//    j=i; }
//
//  return oddNodes; }





using namespace Eigen;
using namespace std;



#define COVERAGE_POINTS		72



void SatelliteCoverageMapLayer::resize(QResizeEvent* event){
	const QSize SIZE = event->size();

	safeDelete(virtualMap);
	virtualMap = new QPixmap(SIZE*3);

	left   = QRect(QPoint(0, SIZE.height()), SIZE);
	right  = QRect(QPoint(SIZE.width()*2, SIZE.height()), SIZE);
	top    = QRect(QPoint(SIZE.width(), 0), SIZE);
	bottom = QRect(QPoint(SIZE.width(), SIZE.height()*2), SIZE);
	center = QRect(QPoint(SIZE.width(), SIZE.height()), SIZE);
}



void SatelliteCoverageMapLayer::render(QPainter& painter){
	for(std::size_t i=0; i<SatellitesManager::getInstance()->size(); i++)
		renderSatellite(painter, SatellitesManager::getInstance()->getSatellite(i)->getLastPosition());
//	for(SatellitesVector::iterator it=satellites->begin(); it!=satellites->end(); it++)
//		renderSatellite(painter, (*it)->getLastPosition());
}



int SatelliteCoverageMapLayer::longitudeToVirtualPixel(float longitude){
	return (int)(getMap()->width()*(longitude+M_PI)/(2.0f*M_PI));
}



int SatelliteCoverageMapLayer::latitudeToVirtualPixel(float latitude){
	return (int)(getMap()->height()*(-latitude+M_PI/2.0f + M_PI)/M_PI);
}



void SatelliteCoverageMapLayer::renderSatellite(QPainter& painter, SatellitePosition position){
	//const float ANGLE_STEP = 2*M_PI/COVERAGE_POINTS;
	QPointF points[COVERAGE_POINTS];

	const float
		EARTH_RADIUS     = 6371.0009f,
		SATELLITE_RADIUS = EARTH_RADIUS + position.getAltitude(),
		CIRCLE_DIST      = EARTH_RADIUS*EARTH_RADIUS / SATELLITE_RADIUS,
		CIRCLE_RADIUS    = EARTH_RADIUS*sqrt(SATELLITE_RADIUS*SATELLITE_RADIUS-EARTH_RADIUS*EARTH_RADIUS)/SATELLITE_RADIUS;

	const Vector3f SATELLITE(
		SATELLITE_RADIUS*sin(M_PI/2.0f+position.getLatitude())*cos(position.getLongitude()),
		SATELLITE_RADIUS*sin(M_PI/2.0f+position.getLatitude())*sin(position.getLongitude()),
		SATELLITE_RADIUS*cos(M_PI/2.0f+position.getLatitude())),

		CIRCLE_CENTER = SATELLITE.normalized()*CIRCLE_DIST,
		P             = CIRCLE_CENTER + Vector3f(100.0f, 100.0f, 100.0f),
		R             = P.cross(CIRCLE_CENTER).normalized(),
		S             = R.cross(P).normalized();

	virtualMap->fill(Qt::transparent);
	QPainter virtualPainter(virtualMap);
	virtualPainter.setBrush(QColor(255, 255, 0, 16));
	virtualPainter.setPen(QPen(QColor(255, 255, 0), 1));

	for(int i=0; i<COVERAGE_POINTS; i++){
		const float ANGLE = (2.0f*M_PI) / COVERAGE_POINTS * i;
		const Vector3f POINT(
			CIRCLE_CENTER(0) + CIRCLE_RADIUS*cos(ANGLE)*R(0) + CIRCLE_RADIUS*sin(ANGLE)*S(0),
			CIRCLE_CENTER(1) + CIRCLE_RADIUS*cos(ANGLE)*R(1) + CIRCLE_RADIUS*sin(ANGLE)*S(1),
			CIRCLE_CENTER(2) + CIRCLE_RADIUS*cos(ANGLE)*R(2) + CIRCLE_RADIUS*sin(ANGLE)*S(2));

		float
			LONGITUDE = POINT(0) == 0.0f ? 0.0f : atan2(POINT(1), POINT(0)),
			LATITUDE  = acos(POINT(2)/EARTH_RADIUS) - M_PI/2.0f;

		LONGITUDE = LONGITUDE < 0.0f ? LONGITUDE + 2.0f*M_PI : LONGITUDE;

		points[i].setX(longitudeToVirtualPixel(LONGITUDE));
		points[i].setY(latitudeToVirtualPixel(LATITUDE));
	}

	//virtualPainter.drawConvexPolygon(points, COVERAGE_POINTS);
	//virtualPainter.drawPoints(points, COVERAGE_POINTS);
	virtualPainter.setBrush(QColor(255, 255, 0));
	virtualPainter.setPen(QColor(0, 0, 0));
	for(int i=0; i<COVERAGE_POINTS; i++){
		int x = (int)points[i].x(), y = (int)points[i].y();
		if(x < 3 || y < 3 || x+3 >= virtualMap->width() || y+3 >= virtualMap->height())
			continue;
		virtualPainter.drawRect(x-1, y-1, 2, 2);
	}

	const int
		W = getMap()->width(),
		H = getMap()->height();
	virtualPainter.drawPixmap(center, *virtualMap, left);
	virtualPainter.drawPixmap(center, *virtualMap, right);
	virtualPainter.drawPixmap(center, *virtualMap, top);
	virtualPainter.drawPixmap(center, *virtualMap, bottom);

	painter.drawPixmap(0, 0, W, H, *virtualMap, W, H, W, H);

	//virtualPainter.fillRect(W, H, W, H, QColor(127,127,127,127));
	//painter.drawPixmap(0, 0, W, H, *virtualMap, 0, 0, virtualMap->width(), virtualMap->height());
}
