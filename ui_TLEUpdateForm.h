/********************************************************************************
** Form generated from reading UI file 'TLEUpdateForm.ui'
**
** Created: Wed Jul 24 20:56:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TLEUPDATEFORM_H
#define UI_TLEUPDATEFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_TLEUpdateForm
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *sourcesEdit;
    QListWidget *sourcesList;
    QPushButton *plusButton;
    QPushButton *minusButton;
    QLabel *label;
    QProgressBar *updateProgress;
    QPushButton *sortButton;

    void setupUi(QDialog *TLEUpdateForm)
    {
        if (TLEUpdateForm->objectName().isEmpty())
            TLEUpdateForm->setObjectName(QString::fromUtf8("TLEUpdateForm"));
        TLEUpdateForm->setWindowModality(Qt::WindowModal);
        TLEUpdateForm->resize(641, 327);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TLEUpdateForm->sizePolicy().hasHeightForWidth());
        TLEUpdateForm->setSizePolicy(sizePolicy);
        TLEUpdateForm->setModal(true);
        buttonBox = new QDialogButtonBox(TLEUpdateForm);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 290, 621, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(false);
        sourcesEdit = new QLineEdit(TLEUpdateForm);
        sourcesEdit->setObjectName(QString::fromUtf8("sourcesEdit"));
        sourcesEdit->setGeometry(QRect(10, 30, 511, 27));
        sourcesList = new QListWidget(TLEUpdateForm);
        sourcesList->setObjectName(QString::fromUtf8("sourcesList"));
        sourcesList->setGeometry(QRect(10, 60, 621, 192));
        plusButton = new QPushButton(TLEUpdateForm);
        plusButton->setObjectName(QString::fromUtf8("plusButton"));
        plusButton->setGeometry(QRect(520, 30, 31, 27));
        minusButton = new QPushButton(TLEUpdateForm);
        minusButton->setObjectName(QString::fromUtf8("minusButton"));
        minusButton->setGeometry(QRect(550, 30, 31, 27));
        label = new QLabel(TLEUpdateForm);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 291, 17));
        updateProgress = new QProgressBar(TLEUpdateForm);
        updateProgress->setObjectName(QString::fromUtf8("updateProgress"));
        updateProgress->setGeometry(QRect(10, 260, 621, 23));
        updateProgress->setValue(24);
        sortButton = new QPushButton(TLEUpdateForm);
        sortButton->setObjectName(QString::fromUtf8("sortButton"));
        sortButton->setGeometry(QRect(580, 30, 51, 27));

        retranslateUi(TLEUpdateForm);
        QObject::connect(buttonBox, SIGNAL(accepted()), TLEUpdateForm, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TLEUpdateForm, SLOT(reject()));

        QMetaObject::connectSlotsByName(TLEUpdateForm);
    } // setupUi

    void retranslateUi(QDialog *TLEUpdateForm)
    {
        TLEUpdateForm->setWindowTitle(QApplication::translate("TLEUpdateForm", "TLE updater", 0, QApplication::UnicodeUTF8));
        sourcesEdit->setText(QString());
        plusButton->setText(QApplication::translate("TLEUpdateForm", "+", 0, QApplication::UnicodeUTF8));
        minusButton->setText(QApplication::translate("TLEUpdateForm", "-", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TLEUpdateForm", "Update sources", 0, QApplication::UnicodeUTF8));
        updateProgress->setFormat(QApplication::translate("TLEUpdateForm", "%v/%m", 0, QApplication::UnicodeUTF8));
        sortButton->setText(QApplication::translate("TLEUpdateForm", "Sort", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TLEUpdateForm: public Ui_TLEUpdateForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TLEUPDATEFORM_H
