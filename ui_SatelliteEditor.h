/********************************************************************************
** Form generated from reading UI file 'SatelliteEditor.ui'
**
** Created: Wed Jul 24 20:56:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SATELLITEEDITOR_H
#define UI_SATELLITEEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_SatelliteEditor
{
public:
    QDialogButtonBox *buttonBox;
    QDoubleSpinBox *uploadSpin;
    QDoubleSpinBox *downloadSpin;
    QLabel *label;
    QLabel *label_2;
    QSpinBox *prioritySpin;
    QLabel *label_3;

    void setupUi(QDialog *SatelliteEditor)
    {
        if (SatelliteEditor->objectName().isEmpty())
            SatelliteEditor->setObjectName(QString::fromUtf8("SatelliteEditor"));
        SatelliteEditor->setWindowModality(Qt::ApplicationModal);
        SatelliteEditor->resize(386, 153);
        buttonBox = new QDialogButtonBox(SatelliteEditor);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 110, 371, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        uploadSpin = new QDoubleSpinBox(SatelliteEditor);
        uploadSpin->setObjectName(QString::fromUtf8("uploadSpin"));
        uploadSpin->setGeometry(QRect(240, 10, 141, 27));
        uploadSpin->setDecimals(6);
        uploadSpin->setMinimum(0);
        uploadSpin->setMaximum(1000);
        uploadSpin->setValue(0);
        downloadSpin = new QDoubleSpinBox(SatelliteEditor);
        downloadSpin->setObjectName(QString::fromUtf8("downloadSpin"));
        downloadSpin->setGeometry(QRect(240, 40, 141, 27));
        downloadSpin->setDecimals(6);
        downloadSpin->setMinimum(0);
        downloadSpin->setMaximum(1000);
        downloadSpin->setValue(0);
        label = new QLabel(SatelliteEditor);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 15, 211, 17));
        label_2 = new QLabel(SatelliteEditor);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 45, 210, 17));
        prioritySpin = new QSpinBox(SatelliteEditor);
        prioritySpin->setObjectName(QString::fromUtf8("prioritySpin"));
        prioritySpin->setGeometry(QRect(240, 70, 141, 27));
        prioritySpin->setMaximum(10000);
        label_3 = new QLabel(SatelliteEditor);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 75, 210, 17));

        retranslateUi(SatelliteEditor);
        QObject::connect(buttonBox, SIGNAL(accepted()), SatelliteEditor, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SatelliteEditor, SLOT(reject()));

        QMetaObject::connectSlotsByName(SatelliteEditor);
    } // setupUi

    void retranslateUi(QDialog *SatelliteEditor)
    {
        SatelliteEditor->setWindowTitle(QApplication::translate("SatelliteEditor", "Satellite editor", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SatelliteEditor", "Upload frequency [MHz]", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SatelliteEditor", "Download frequency [MHz]", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SatelliteEditor", "Priority", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SatelliteEditor: public Ui_SatelliteEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SATELLITEEDITOR_H
