/*
 * File:   TLEUpdateListener.h
 * Author: peter
 *
 * Created on October 20, 2012, 1:31 PM
 */

#ifndef TLEUPDATELISTENER_H
#define	TLEUPDATELISTENER_H



#include "TLE.h"



class TLEUpdateListener{
public:
	virtual ~TLEUpdateListener() { }

	virtual void update(const TLE& tle) = 0;
};


#endif	/* TLEUPDATELISTENER_H */

