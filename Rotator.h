/*
 * File:   Rotator.h
 * Author: peter
 *
 * Created on December 29, 2012, 2:00 PM
 */

#ifndef ROTATOR_H
#define	ROTATOR_H



#include "Singleton.h"


#include <ptypes/pinet.h>
#include <stdexcept>
#include <string>



class Rotator : public Singleton<Rotator>{
public:
	void pointTo(double azimuth, double elevation);

	std::string getLastPosition();

	void connect(const std::string& address);
	void disconnect();
	inline bool isConnected() const { return connected; }
	class RotatorError : public std::runtime_error{
	public:
		RotatorError(const std::string& error) :
			std::runtime_error(error.c_str()) { }
	};

private:
	Rotator() : connected(false) { }
	~Rotator() { disconnect(); }
	friend class Singleton<Rotator>;

	bool connected;
	PTYPES_NAMESPACE::ipstream stream;

	void cleanInput();
};



#endif	/* ROTATOR_H */
