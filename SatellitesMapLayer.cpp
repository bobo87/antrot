#include "SatellitesMapLayer.h"

#include "SatellitesManager.h"
#include "Satellite.h"



void SatellitesMapLayer::render(QPainter& painter){
	if(!satellites)
		return;

	painter.setPen(QColor(Qt::green));
	painter.setBrush(QColor(Qt::black));
//	for(size_t i=0; i<satellites->size(); i++){
//		Satellite* satellite = satellites->at(i);
	for(size_t i=0; i<SatellitesManager::getInstance()->size(); i++){
		Satellite* satellite = SatellitesManager::getInstance()->getSatellite(i);

		const int X = longitudeToPixel(satellite->getLastPosition().getLongitude()),
				  Y = latitudeToPixel(satellite->getLastPosition().getLatitude());

		painter.drawPixmap(
				X - icon.width()/2,
				Y - icon.height()/2,
				icon);

		painter.drawText(X, Y+icon.height(), satellite->getTLE().getName().c_str());

//		QPainterPath path;
//		path.addText(X, Y+icon.height(), painter.font(), satellite->getTLE().getName().c_str());
//		painter.drawPath(path);
	}
}


