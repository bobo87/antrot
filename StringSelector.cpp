/*
 * File:   TLESelector.cpp
 * Author: peter
 *
 * Created on January 8, 2013, 11:50 PM
 */

#include "StringSelector.h"



using namespace std;



StringSelector::StringSelector(const StringVector& strings){
	widget.setupUi(this);
	for(StringVector::const_iterator it=strings.begin(); it!=strings.end(); it++)
		widget.nameCombo->addItem((*it).c_str());
}



StringSelector::~StringSelector(){
}



std::string StringSelector::getString(const StringVector& strings){
	StringSelector *selector = new StringSelector(strings);
	string ret = "";

	if(selector->exec() == QDialog::Accepted)
		ret = selector->getSelected();

	delete selector;
	return ret;
}
