/*
 * File:   SatellitePosition.h
 * Author: peter
 *
 * Created on October 10, 2012, 8:06 PM
 */

#ifndef SATELLITEPOSITION_H
#define	SATELLITEPOSITION_H



#include <string>



class SatellitePosition{
public:
	/**
	 * create satellite position data holder
	 *
     * @param azimuth azimuth in radians
     * @param elevation elevation in radians
     * @param latitude latitude in radians
     * @param longitude longitude in radians
     * @param range range in km
     * @param rangeRate range rate in m/s
     * @param phase phase in 1/256
     * @param altitude altitude in km
     * @param theta theta in radians/s
     * @param aboveHorizon is above horizon?
     */
	SatellitePosition(double azimuth, double elevation, double latitude, double longitude,
			double range, double rangeRate, double phase, double altitude,
			double theta) :

			azimuth(azimuth),
			elevation(elevation),
			latitude(latitude),
			longitude(longitude),
			range(range),
			rangeRate(rangeRate),
			phase(phase),
			altitude(altitude),
			theta(theta),
			//aboveHorizon(aboveHorizon) { }
				aboveHorizon(elevation > 0.0f) { }

	inline double getAzimuth() const { return azimuth; }
	inline double getElevation() const { return elevation; }
	inline double getLatitude() const { return latitude; }
	inline double getLongitude() const { return longitude; }
	inline double getRange() const { return range; }
	inline double getRangeRate() const { return rangeRate; }
	inline double getPhase() const { return phase; }
	inline double getAltitude() const { return altitude; }
	inline double getTheta() const { return theta; }
	inline bool   isAboveHorizon() const { return aboveHorizon; }

private:

	double azimuth, elevation,
			latitude, longitude,
			range, rangeRate,
			phase, altitude,
			theta;
	bool aboveHorizon;
};

#endif	/* SATELLITEPOSITION_H */

