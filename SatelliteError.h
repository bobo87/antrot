/*
 * File:   SatelliteError.h
 * Author: peter
 *
 * Created on October 10, 2012, 11:21 PM
 */

#ifndef SATELLITEERROR_H
#define	SATELLITEERROR_H



#include <stdexcept>
#include <string>



class SatelliteError : public std::runtime_error{
public:
	SatelliteError(const std::string& error) :
		std::runtime_error(error.c_str()) { }
};

#endif	/* SATELLITEERROR_H */

