/*
 * File:   SatellitePassTime.h
 * Author: peter
 *
 * Created on October 10, 2012, 9:15 PM
 */

#ifndef TIMESLOT_H
#define	TIMESLOT_H



#include "TimeBase.h"

#include <ctime>
#include <stdint.h>



class TimeSlot{
public:
	TimeSlot(UnixTime startTime = 0, UnixTime endTime = 0) :
		startTime(startTime), endTime(endTime) { }

	inline UnixTime getStartTime() const { return startTime; }
	inline UnixTime getEndTime() const { return endTime; }
	inline UnixTime getDuration() const { return endTime-startTime; }

	inline bool isTimePoint() const { return (startTime == endTime) && startTime; }
	inline bool isNull() const { return (startTime == endTime) == 0; }

private:
	UnixTime startTime, endTime;
};
#endif	/* SATELLITEPASSTIME_H */
