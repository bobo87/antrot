/*
 * File:   Satellite.h
 * Author: peter
 *
 * Created on October 13, 2012, 10:28 PM
 */

#ifndef SATELLITE_H
#define	SATELLITE_H



#include "GroundStationPositionListener.h"
#include "Trajectory.h"
#include "SatellitePredictor.h"

#include <QMutex>
#include <QMutexLocker>



class Satellite : public GroundStationPositionListener{
public:
	Satellite(const TLE& tle, int priority = 0);

	inline int getPriority() const{
		return priority;
	}

	inline void setPriority(int priority){
		this->priority = priority;
	}

	inline TLE getTLE() {
		QMutexLocker locker(&tleMutex);
		return tle;
	}
	SatellitePosition getPosition(std::time_t time = 0);
	TimeSlot getNextPass(std::time_t time = 0);

	FrequencyPair getDopplerFrequencies(std::time_t time = 0);

	inline FrequencyPair getFrequencyPair() const{
		return frequency;
	}

	inline void setFrequencyPair(const FrequencyPair& frequencyPair){
		frequency = frequencyPair;
	}

	inline void unloadPredictor() { if(predictor) { delete predictor; predictor = 0; } }

	inline void updateTLE(const TLE& tle){
		QMutexLocker locker(&tleMutex);
		unloadPredictor();
		this->tle = tle;
	}

	SatellitePosition getLastPosition();

	inline const Trajectory& getTrajectory() const { return trajectory; }
	inline void recordLastPosition() { trajectory.recordPoint(getLastPosition()); }

	void onGroundStationPositionChanged(GroundStationPosition newPosition);
	
private:
	QMutex tleMutex;

	TLE tle;

	SatellitePredictor *predictor;
	SatellitePosition *lastPosition;

	Trajectory trajectory;

	int priority;
	FrequencyPair frequency;
};


#endif	/* SATELLITE_H */

