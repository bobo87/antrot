#include "DaylightMapLayer.h"



DaylightMapLayer::DaylightMapLayer(WorldMap *map) :
	MapLayer(map),
	deadTime(60*1),
	lastRender(0){
}


void DaylightMapLayer::render(QPainter& painter){
	const UnixTime CURRENT_TIME = TimeBase::getInstance()->getTime();
	if(!generator.getPixmap()){
		painter.drawPixmap(0, 0, *generator.render(lastRender = CURRENT_TIME));
		return;
	}

	if(deadTime > (CURRENT_TIME-lastRender)){
		painter.drawPixmap(0, 0, *generator.getPixmap());
		return;
	}

	lastRender = CURRENT_TIME;
	painter.drawPixmap(0, 0, *generator.render(CURRENT_TIME));
}
