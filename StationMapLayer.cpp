#include "StationMapLayer.h"



void StationMapLayer::render(QPainter& painter){
	painter.drawPixmap(
			longitudeToPixel(station.getLongitudeRadians()) - icon.width()/2,
			latitudeToPixel(station.getLatitudeRadians()) - icon.height()/2,
			icon);
}

