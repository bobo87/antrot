#include "Tracker.h"

#include "SatellitesManager.h"

#include "Rotator.h"
#include "Settings.h"

#include <iostream>



using namespace std;



Satellite* Tracker::getTrackedSatellite(){
	QMutexLocker locker(&satChangeMutex);
	return trackedSatellite;
}


void Tracker::setTrackedSatellite(Satellite *trackedSatellite){
	QMutexLocker locker(&satChangeMutex);
	if(this->trackedSatellite)
		this->trackedSatellite->unloadPredictor();
	this->trackedSatellite = trackedSatellite;
	satelliteChanged = true;
	if(!trackedSatellite->getPosition().isAboveHorizon())
		prepareNextPass();

	QMutexLocker locker2(&stateMutex);
	state = tsWaiting;
}



void Tracker::run(){
	QMutexLocker runningLocker(&runningMutex);
	QMutexLocker stateLocker(&stateMutex);
	state            = tsWaiting;
	running          = true;
	satelliteChanged = false;
	stateLocker.unlock();

	cout << "Tracker active" << endl;
	while(running){
		runningLocker.unlock();
		msleep(Settings::getInstance()->getTrackingInterval());

		satChangeMutex.lock();
		if(satelliteChanged){
			cout << "Tracked satellite changed" << endl;
			// TODO
			satelliteChanged = false;
		}

		if(!trackedSatellite){
			satChangeMutex.unlock();
			continue;
		}

		SatellitePosition position = trackedSatellite->getPosition(TimeBase::getInstance()->getTime());
		satChangeMutex.unlock();

		switch(state){
			case tsWaiting:
				if(position.isAboveHorizon()){
					stateLocker.relock();
					state = tsTracking;
					stateLocker.unlock();
					cout << "Satellite is now visible" << endl;
				}
			break;

			case tsTracking:
				if(!position.isAboveHorizon()){
					stateLocker.relock();
					state = tsWaiting;
					stateLocker.unlock();
					cout << "Satellite is now not visible" << endl;
					prepareNextPass();
				}
			break;

			default:
				continue;
		};

		if(state == tsTracking)
			Rotator::getInstance()->pointTo(position.getAzimuth(), position.getElevation());

		runningLocker.relock();
	}
	cout << "Tracker done" << endl;
}



void Tracker::prepareNextPass(){
//	if(!trackedSatellite)
//		return;

	SatellitesManager *satman = SatellitesManager::getInstance();
	UnixTime firstTime = 0;
	size_t first = 0;
	for(size_t i=0; i<satman->size(); i++){
		UnixTime passStart = satman->getSatellite(i)->getNextPass().getStartTime();
		if(!firstTime || passStart<firstTime){
			firstTime = passStart;
			first = i;
		}
	}
	trackedSatellite = satman->getSatellite(first);

	TimeSlot          passTime = trackedSatellite->getNextPass();
	SatellitePosition position = trackedSatellite->getPosition(passTime.getStartTime());
	cout << "Next pass of " << trackedSatellite->getTLE().getName() << " from " <<
		timeToString(passTime.getStartTime()) << " to " << timeToString(passTime.getEndTime()) << endl;
	Rotator::getInstance()->pointTo(position.getAzimuth(), position.getElevation());
}
