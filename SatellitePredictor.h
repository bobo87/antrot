/*
 * File:   SatellitePredictor.h
 * Author: peter
 *
 * Created on October 10, 2012, 10:39 PM
 */

#ifndef SATELLITEPREDICTOR_H
#define	SATELLITEPREDICTOR_H



#include "SatellitePosition.h"
#include "TimeSlot.h"
#include "FrequencyPair.h"
#include "TLE.h"
#include "GroundStationPosition.h"
#include "SatelliteError.h"

#include <ptypes/pinet.h>
#include <string>
#include <ctime>
#include <list>

#include <QMutex>
#include <QMutexLocker>



class SatellitePredictor{
	static const char* DEFAULT_SERVER;
	static const int   DEFAULT_PORT;
	static const int   DEFAULT_TIMEOUT;
public:

	SatellitePredictor(const GroundStationPosition& groundStation, const TLE& tle,
		               const std::string& serverName = DEFAULT_SERVER, int port = DEFAULT_PORT);
	~SatellitePredictor();

	SatellitePosition getPosition();
	TimeSlot getNextPass();
	FrequencyPair getFrequencyPair(Frequency base);
	void setTime(UnixTime unixTime = 0);

private:

	QMutex inUse;

	std::string readLine();
	void checkReply();

	PTYPES_NAMESPACE::ipstream stream;

	UnixTime currentTime;
	struct CachedPosition {
		UnixTime time;
		SatellitePosition position;

		CachedPosition() :
			time(0),
			position(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) { }
	};
	typedef std::list<CachedPosition> CacheType;
	CacheType positionCache;
	static const std::size_t CACHE_SIZE;
};


#endif	/* SATELLITEPREDICTOR_H */
