/*
 * File:   Tracker.h
 * Author: peter
 *
 * Created on December 13, 2012, 11:39 PM
 */

#ifndef TRACKER_H
#define	TRACKER_H



#include "Satellite.h"

#include <QMutex>
#include <QThread>
#include <QMutexLocker>



class Tracker : public QThread{
public:

	enum TrackerState { tsWaiting, tsTracking } ;

	Satellite* getTrackedSatellite();
	void setTrackedSatellite(Satellite *trackedSatellite);

	inline void quit() { QMutexLocker locker(&runningMutex); running = false; }
	inline TrackerState getState() { QMutexLocker locker(&stateMutex); return state; }


protected:
	void run();

private:
	Satellite *trackedSatellite;
	bool satelliteChanged;
	QMutex satChangeMutex;

	QMutex runningMutex;
	bool running;
	QMutex stateMutex;
	TrackerState state;

	void prepareNextPass();
};



#endif	/* TRACKER_H */
