/*
 * File:   TrackingPlan.h
 * Author: peter
 *
 * Created on January 5, 2013, 2:17 PM
 */

#ifndef TRACKINGPLAN_H
#define	TRACKINGPLAN_H



#include "TrackedPass.h"



class TrackingPlan{
public:
	TrackingPlan() { }
	virtual ~TrackingPlan() { }

	virtual TrackedPass getNextPass() = 0;
private:
};



#endif	/* TRACKINGPLAN_H */
