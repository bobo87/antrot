/********************************************************************************
** Form generated from reading UI file 'TLEEditForm.ui'
**
** Created: Wed Jul 24 20:56:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TLEEDITFORM_H
#define UI_TLEEDITFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_TLEEditForm
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *nameEdit;
    QLabel *label_2;
    QLineEdit *line1Edit;
    QLabel *label_3;
    QLineEdit *line2Edit;
    QLabel *validityLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *TLEEditForm)
    {
        if (TLEEditForm->objectName().isEmpty())
            TLEEditForm->setObjectName(QString::fromUtf8("TLEEditForm"));
        TLEEditForm->resize(713, 170);
        TLEEditForm->setSizeGripEnabled(true);
        TLEEditForm->setModal(true);
        gridLayout = new QGridLayout(TLEEditForm);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(TLEEditForm);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        nameEdit = new QLineEdit(TLEEditForm);
        nameEdit->setObjectName(QString::fromUtf8("nameEdit"));
        QFont font;
        font.setFamily(QString::fromUtf8("Monospace"));
        nameEdit->setFont(font);

        gridLayout->addWidget(nameEdit, 0, 1, 1, 1);

        label_2 = new QLabel(TLEEditForm);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        line1Edit = new QLineEdit(TLEEditForm);
        line1Edit->setObjectName(QString::fromUtf8("line1Edit"));
        line1Edit->setFont(font);

        gridLayout->addWidget(line1Edit, 1, 1, 1, 1);

        label_3 = new QLabel(TLEEditForm);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        line2Edit = new QLineEdit(TLEEditForm);
        line2Edit->setObjectName(QString::fromUtf8("line2Edit"));
        line2Edit->setFont(font);

        gridLayout->addWidget(line2Edit, 2, 1, 1, 1);

        validityLabel = new QLabel(TLEEditForm);
        validityLabel->setObjectName(QString::fromUtf8("validityLabel"));
        validityLabel->setFrameShadow(QFrame::Plain);
        validityLabel->setTextFormat(Qt::RichText);
        validityLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(validityLabel, 3, 1, 1, 1);

        buttonBox = new QDialogButtonBox(TLEEditForm);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(false);

        gridLayout->addWidget(buttonBox, 4, 0, 1, 2);


        retranslateUi(TLEEditForm);
        QObject::connect(buttonBox, SIGNAL(accepted()), TLEEditForm, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TLEEditForm, SLOT(reject()));

        QMetaObject::connectSlotsByName(TLEEditForm);
    } // setupUi

    void retranslateUi(QDialog *TLEEditForm)
    {
        TLEEditForm->setWindowTitle(QApplication::translate("TLEEditForm", "TLE Editor", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TLEEditForm", "Name", 0, QApplication::UnicodeUTF8));
        nameEdit->setText(QString());
        label_2->setText(QApplication::translate("TLEEditForm", "Line 1", 0, QApplication::UnicodeUTF8));
        line1Edit->setText(QString());
        label_3->setText(QApplication::translate("TLEEditForm", "Line 2", 0, QApplication::UnicodeUTF8));
        line2Edit->setText(QString());
        validityLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class TLEEditForm: public Ui_TLEEditForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TLEEDITFORM_H
