/*
 * File:   GridMapLayer.h
 * Author: peter
 *
 * Created on October 13, 2012, 5:49 PM
 */

#ifndef GRIDMAPLAYER_H
#define	GRIDMAPLAYER_H



#include "MapLayer.h"



class GridMapLayer : public MapLayer{
public:
	GridMapLayer(WorldMap *map) :
		MapLayer(map) { }

	void render(QPainter& painter);
};


#endif	/* GRIDMAPLAYER_H */

