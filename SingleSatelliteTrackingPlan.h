/*
 * File:   SingleSatelliteTrackingPlan.h
 * Author: peter
 *
 * Created on January 5, 2013, 2:34 PM
 */

#ifndef SINGLESATELLITETRACKINGPLAN_H
#define	SINGLESATELLITETRACKINGPLAN_H



#include "TrackingPlan.h"
#include "Satellite.h"



class SingleSatelliteTrackingPlan : public TrackingPlan{
public:
	SingleSatelliteTrackingPlan(Satellite *satellite = 0) : satellite(satellite) { }

	TrackedPass getNextPass();

	inline void setSatellite(Satellite *satellite) { this->satellite = satellite; }
	inline Satellite* getSatellite() const { return satellite; }

private:
	Satellite *satellite;
};



#endif	/* SINGLESATELLITETRACKINGPLAN_H */
