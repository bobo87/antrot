/*
 * File:   SatelliteCoverageMapLayer.h
 * Author: peter
 *
 * Created on November 1, 2012, 9:01 PM
 */

#ifndef SATELLITECOVERAGEMAPLAYER_H
#define	SATELLITECOVERAGEMAPLAYER_H



#include "MapLayer.h"
#include "SatellitePosition.h"
#include "misc.h"



class SatelliteCoverageMapLayer : public MapLayer{
public:
	SatelliteCoverageMapLayer(WorldMap *map) :
		MapLayer(map), satellites(0), virtualMap(0) { }

	void render(QPainter& painter);

	inline void setSatellitesVector(SatellitesVector *satellites) { this->satellites = satellites; }

	void resize(QResizeEvent* event);

	int longitudeToVirtualPixel(float longitude);
	int latitudeToVirtualPixel(float latitude);

private:
	void renderSatellite(QPainter& painter, SatellitePosition position);

	SatellitesVector* satellites;

	QPixmap *virtualMap;
	QRect left, right, top, bottom, center;
};



#endif	/* SATELLITECOVERAGEMAPLAYER_H */
