/*
 * File:   DaylightMaskGenerator.h
 * Author: peter
 *
 * Created on October 27, 2012, 11:13 PM
 */

#ifndef DAYLIGHTMASKGENERATOR_H
#define	DAYLIGHTMASKGENERATOR_H



#include "TimeSlot.h"

#include <ctime>
#include <QPainter>
#include <QPixmap>



class DaylightMaskGenerator{
public:
	DaylightMaskGenerator();

	~DaylightMaskGenerator();

	inline QPixmap* getPixmap() const {
		return result;
	}

	QPixmap* render(UnixTime renderTime);

	void setSize(int width, int height);

	static void loadImages();
	static void releaseImages();

private:
	void precalc(UnixTime renderTime);
	void renderLightmap(QPainter& painter);

	static QPixmap *sun, *nightMap;

	QPixmap *result, *resizedMap;

	float declination, sunLongitude;
	std::tm utc;
	float dayTime;
};



#endif	/* DAYLIGHTMASKGENERATOR_H */
