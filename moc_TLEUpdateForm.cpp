/****************************************************************************
** Meta object code from reading C++ file 'TLEUpdateForm.h'
**
** Created: Wed Jul 24 20:57:25 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "TLEUpdateForm.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TLEUpdateForm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TLEUpdateForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   15,   14,   14, 0x0a,
      39,   14,   14,   14, 0x0a,
      52,   14,   14,   14, 0x0a,
      66,   14,   14,   14, 0x0a,
      79,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TLEUpdateForm[] = {
    "TLEUpdateForm\0\0row\0sourceSelected(int)\0"
    "plusButton()\0minusButton()\0sortButton()\0"
    "accepted()\0"
};

void TLEUpdateForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TLEUpdateForm *_t = static_cast<TLEUpdateForm *>(_o);
        switch (_id) {
        case 0: _t->sourceSelected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->plusButton(); break;
        case 2: _t->minusButton(); break;
        case 3: _t->sortButton(); break;
        case 4: _t->accepted(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TLEUpdateForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TLEUpdateForm::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TLEUpdateForm,
      qt_meta_data_TLEUpdateForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TLEUpdateForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TLEUpdateForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TLEUpdateForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TLEUpdateForm))
        return static_cast<void*>(const_cast< TLEUpdateForm*>(this));
    return QDialog::qt_metacast(_clname);
}

int TLEUpdateForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
