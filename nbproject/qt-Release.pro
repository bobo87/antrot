# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/GNU-Linux-x86
TARGET = AntRot
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui
SOURCES += StationMapLayer.cpp WorldMap.cpp MainWindow.cpp Radar.cpp Settings.cpp Trajectory.cpp TimeBase.cpp DaylightMaskGenerator.cpp main.cpp SatellitePredictor.cpp TrajectoryMapLayer.cpp Satellite.cpp GridMapLayer.cpp TLEManager.cpp pugixml.cpp SatellitesManager.cpp FileDownloader.cpp SatelliteEditor.cpp TLEFileUpdate.cpp DaylightMapLayer.cpp MapLayer.cpp misc.cpp TLE.cpp StringSelector.cpp TLEEditForm.cpp Tracker.cpp TimeSlot.cpp SatelliteCoverageMapLayer.cpp TLEUpdateSource.cpp Rotator.cpp TLEUpdateForm.cpp SatellitesMapLayer.cpp SingleSatelliteTrackingPlan.cpp GroundStationPosition.cpp
HEADERS += Radar.h TLEManager.h SatellitePredictor.h TLEEditForm.h TimeSlot.h Tracker.h TrajectoryMapLayer.h TrackingPlan.h GroundStationPositionListener.h misc.h SatellitesManager.h SatellitePosition.h TLEFileUpdate.h TLEUpdateSource.h pugiconfig.hpp TLEUpdateForm.h GridMapLayer.h FrequencyPair.h Satellite.h SatellitesMapLayer.h Settings.h SatelliteEditor.h SatelliteCoverageMapLayer.h GroundStationPosition.h Trajectory.h WorldMap.h TLEUpdateListener.h TimeBase.h MainWindow.h StringSelector.h Singleton.h SatelliteError.h TLE.h FileDownloader.h pugixml.hpp TrackedPass.h StationMapLayer.h DaylightMapLayer.h SingleSatelliteTrackingPlan.h MapLayer.h Rotator.h DaylightMaskGenerator.h
FORMS += SatelliteEditor.ui TLEUpdateForm.ui TLESelector.ui TLEEditForm.ui MainWindow.ui
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += -lptypes  
