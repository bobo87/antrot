#include "SatellitePredictor.h"

#include <sstream>



using namespace std;
USING_PTYPES;



const char*       SatellitePredictor::DEFAULT_SERVER  = "localhost";
const int         SatellitePredictor::DEFAULT_PORT    = 8080;
const int         SatellitePredictor::DEFAULT_TIMEOUT = 1000;
const std::size_t SatellitePredictor::CACHE_SIZE      = 50;



SatellitePredictor::SatellitePredictor(
		const GroundStationPosition& groundStation, const TLE& tle,
		const std::string& serverName, int port) :
		currentTime(0) {
	QMutexLocker locker(&inUse);
	stream.set_host(serverName.c_str());
	stream.set_port(port);
	try{
		stream.open();
		stream.putf("%f %f %f\n", groundStation.getLatitude(), groundStation.getLongitude(), groundStation.getHeight());
		for(int i=0; i<3; i++)
			stream.putline(tle.getLine(i).c_str());
		stream.flush();
		checkReply();
	}catch(estream* e){
		throw SatelliteError((const char*)e->get_message());
	}
}



SatellitePredictor::~SatellitePredictor(){
	QMutexLocker locker(&inUse);
	stream.putline("quit");
	stream.flush();
	stream.close();
}



std::string SatellitePredictor::readLine(){
	if(!stream.waitfor(DEFAULT_TIMEOUT))
		throw SatelliteError("server read timeout");
	return (const char*)stream.line();
}



void SatellitePredictor::checkReply(){
	const std::string& REPLY = readLine();
	if(REPLY != "OK")
		throw SatelliteError("server problem : " + REPLY);
}



TimeSlot SatellitePredictor::getNextPass(){
	QMutexLocker locker(&inUse);
	try{
		stream.putline("get_next_pass");
		stream.flush();
		const std::string REPLY = readLine();
		checkReply();

		stringstream line(REPLY);
		UnixTime start, end;
		line >> start >> end;
		return TimeSlot(start, end);
	}catch(estream* e){
		throw SatelliteError((const char*)e->get_message());
		return TimeSlot(0, 0);
	}
}



FrequencyPair SatellitePredictor::getFrequencyPair(Frequency base){
	QMutexLocker locker(&inUse);
	try{
		stream.putf("get_frequency %d\n", base);
		stream.flush();
		const std::string REPLY = readLine();
		checkReply();

		stringstream line(REPLY);
		UnixTime downlink, uplink;
		line >> downlink >> uplink;
		return FrequencyPair(downlink, uplink);
	}catch(estream* e){
		throw SatelliteError((const char*)e->get_message());
		return FrequencyPair(0, 0);
	}
}



void SatellitePredictor::setTime(UnixTime unixTime){
	QMutexLocker locker(&inUse);
	try{
		if(unixTime)
			currentTime = unixTime;
		else
			currentTime = TimeBase::getInstance()->getTime();
		stream.putf("set_time %d\n", currentTime);
		stream.flush();
		checkReply();
	}catch(estream* e){
		throw SatelliteError((const char*)e->get_message());
	}
}



SatellitePosition SatellitePredictor::getPosition(){
	QMutexLocker locker(&inUse);
	for(CacheType::const_iterator it=positionCache.begin(); it!=positionCache.end(); it++){
		if((*it).time == currentTime)
			return (*it).position;
	}

	try{
		stream.putline("get_pos");
		stream.flush();
		const std::string REPLY = readLine();
		checkReply();

		stringstream line(REPLY);
		double azimuth, elevation, latitude, longitude,
			range, rangeRate, phase, altitude, theta;
//		bool aboveHorizon;
		line >> azimuth >> elevation >> latitude >> longitude >>
			range >> rangeRate >> phase >> altitude >> theta;
//		std::string tempBool;
//		line >> tempBool;
//		aboveHorizon = tempBool == "true";

//		return SatellitePosition(azimuth, elevation, latitude, longitude,
//			range, rangeRate, phase, altitude, theta);

		SatellitePosition position(azimuth, elevation, latitude, longitude,
			range, rangeRate, phase, altitude, theta);
		CachedPosition cached;
		cached.position = position;
		cached.time     = currentTime;
		positionCache.push_front(cached);
		if(positionCache.size() > CACHE_SIZE)
			positionCache.resize(CACHE_SIZE);

		return position;
	}catch(estream* e){
		throw SatelliteError((const char*)e->get_message());
		return SatellitePosition(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}
}
