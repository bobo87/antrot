#include "SingleSatelliteTrackingPlan.h"



TrackedPass SingleSatelliteTrackingPlan::getNextPass(){
	if(!satellite)
		return TrackedPass();

	return TrackedPass(satellite->getNextPass(), satellite);
}
