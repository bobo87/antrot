/*
 * File:   GroundStationPosition.h
 * Author: peter
 *
 * Created on October 10, 2012, 10:30 PM
 */

#ifndef GROUNDSTATIONPOSITION_H
#define	GROUNDSTATIONPOSITION_H



#include <cmath>
#include <set>



class GroundStationPositionListener;



class GroundStationPosition{
public:
	/**
	 * set ground station position
	 *
     * @param latitude latitude in degrees
     * @param longitude longitude in degrees
     * @param height height above sea level in meters
     */
	GroundStationPosition(double latitude, double longitude, double height) :
		latitude(latitude), longitude(longitude), height(height) { }

	inline double getLatitude() const { return latitude; }
	inline double getLongitude() const { return longitude; }
	inline double getHeight() const { return height; }

	inline double getLatitudeRadians() const { return latitude*M_PI/180.0; }
	inline double getLongitudeRadians() const { return longitude*M_PI/180.0; }

	static void setPosition(GroundStationPosition position);

	inline static GroundStationPosition getPosition(){
		return GroundStationPosition::position;
	}

	static void addListener(GroundStationPositionListener* listener, bool fireNow=false);

	inline static void removeListener(GroundStationPositionListener* listener){
		listeners.erase(listener);
	}

	inline static bool hasListener(GroundStationPositionListener *listener){
		return listeners.find(listener) != listeners.end();
	}

	static void notifyAll();
	
private:
	double latitude, longitude, height;

	static GroundStationPosition position;

	typedef std::set<GroundStationPositionListener*> ListenersSet;
	static ListenersSet listeners;
};

#endif	/* GROUNDSTATIONPOSITION_H */

