#include "SatellitesManager.h"

#include "pugixml.hpp"
#include "TLEManager.h"



using namespace std;
using namespace pugi;



const std::string SatellitesManager::SATELLITES_FILE = "./data/satellites/satellites.xml";



void SatellitesManager::save(){
	xml_document doc;

	for(SatellitesVector::iterator it=satellites.begin(); it!=satellites.end(); it++){
		Satellite *satellite = *it;
		xml_node node = doc.root().append_child("satellite");

		node.append_attribute("name").set_value(satellite->getTLE().getName().c_str());
		node.append_child("priority").append_child(pugi::node_pcdata).set_value(toString(satellite->getPriority()).c_str());
		node.append_child("downlink").append_child(pugi::node_pcdata).set_value(toString(satellite->getFrequencyPair().getDownlink()).c_str());
		node.append_child("uplink").append_child(pugi::node_pcdata).set_value(toString(satellite->getFrequencyPair().getUplink()).c_str());
	}

	doc.save_file(SATELLITES_FILE.c_str());
}



void SatellitesManager::load(){
	xml_document doc;
	xml_parse_result result = doc.load_file(SATELLITES_FILE.c_str());
	if(result.status != status_ok)
		throw LoadingError("can't load '"+SATELLITES_FILE+"', error : "+result.description());

	for(pugi::xml_node tleNode = doc.root().child("satellite"); tleNode; tleNode = tleNode.next_sibling("satellite")){
		string name = tleNode.attribute("name").as_string("");

		TLEManager *tleManager = TLEManager::getInstance();
		if(!tleManager->exist(name)){
			cout << "satellite loading error - '" << name << "' doesn't have TLE data, ignoring this satellite!" << endl;
			continue;
		}

		TLE tle = tleManager->getTLE(name);
		Satellite *satellite = new Satellite(tle, toInt(tleNode.child("priority").child_value()));
		satellite->setFrequencyPair(FrequencyPair(
			toLong(tleNode.child("downlink").child_value()),
			toLong(tleNode.child("uplink").child_value())));
		tle.setSatellite(satellite);
		TLEManager::getInstance()->update(tle);
		satellites.push_back(satellite);
	}
}
