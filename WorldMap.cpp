#include "WorldMap.h"

#include "SatelliteCoverageMapLayer.h"
#include "DaylightMaskGenerator.h"
#include "DaylightMapLayer.h"
#include "TrajectoryMapLayer.h"
#include "SatellitesMapLayer.h"
#include "StationMapLayer.h"
#include "GridMapLayer.h"
#include "misc.h"
#include "SatellitesMapLayer.h"

#include <QPainter>
#include <QResizeEvent>
#include <cstdio>



using namespace std;



WorldMap::WorldMap(QWidget* widget) :
	QWidget(widget),
	mapDay(0),
	mapDayResized(0),
	mapNightResized(0),
	gridLayer(new GridMapLayer(this)),
	stationLayer(new StationMapLayer(this)),
	satellitesLayer(new SatellitesMapLayer(this)),
	trajectoryLayer(new TrajectoryMapLayer(this)),
	daylightLayer(new DaylightMapLayer(this)),
	satelliteCoverageLayer(new SatelliteCoverageMapLayer(this)),
	satellites(0),
	lastSavedTime(0),
	settings(Settings::getInstance()){

	mapDay   = new QPixmap("./data/map/map-day.jpg");

	DaylightMaskGenerator::loadImages();
}



WorldMap::~WorldMap(){
	safeDelete(mapDay);
	safeDelete(mapDayResized);
	safeDelete(mapNightResized);

	safeDelete(gridLayer);
	safeDelete(stationLayer);
	safeDelete(satellitesLayer);
	safeDelete(trajectoryLayer);
	safeDelete(daylightLayer);
	safeDelete(satelliteCoverageLayer);

	DaylightMaskGenerator::releaseImages();
}



void WorldMap::setGroundStationPosition(GroundStationPosition position){
	stationLayer->setStationPosition(position);
}



void WorldMap::setSatellitesVector(SatellitesVector *satellites){
	this->satellites = satellites;
	satellitesLayer->setSatellitesVector(satellites);
	trajectoryLayer->setSatellitesVector(satellites);
	satelliteCoverageLayer->setSatellitesVector(satellites);
}



void WorldMap::resizeEvent(QResizeEvent *event){
	safeDelete(mapDayResized);
	safeDelete(mapNightResized);

	mapDayResized   = new QPixmap(mapDay->scaled(event->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));

	gridLayer->resize(event);
	stationLayer->resize(event);
	satellitesLayer->resize(event);
	trajectoryLayer->resize(event);
	daylightLayer->resize(event);
	satelliteCoverageLayer->resize(event);
}



void WorldMap::paintEvent(QPaintEvent*){
	QImage image(size(), QImage::Format_ARGB32_Premultiplied);
	QPainter painter(&image);

	painter.drawPixmap(0, 0, *mapDayResized);

	daylightLayer->render(painter);
	gridLayer->render(painter);
	stationLayer->render(painter);
	satelliteCoverageLayer->render(painter);
	trajectoryLayer->render(painter);
	satellitesLayer->render(painter);

	painter.end();

	QPainter widgetPainter(this);
	widgetPainter.drawImage(QPoint(0, 0), image);

	UnixTime actualTime = TimeBase::getInstance()->getSystemTime();
	if((actualTime - lastSavedTime) >= settings->getSaveMapInterval()){
		lastSavedTime = actualTime;
		//image.scaledToHeight(200, Qt::SmoothTransformation).save("map.png", 0, 100);
		image.save(settings->getMapFileName().c_str(), 0, 100);
	}
}
