/*
 * File:   TLESelector.h
 * Author: peter
 *
 * Created on January 8, 2013, 11:50 PM
 */

#ifndef _STRINGSELECTOR_H
#define	_STRINGSELECTOR_H

#include "SatellitesManager.h"
#include "TLEManager.h"

#include "ui_TLESelector.h"

#include <string>



class StringSelector : public QDialog {
	Q_OBJECT
public:
	static inline std::string getTLE() {
		return getString(TLEManager::getInstance()->getSatelliteNames());
	}
	static inline std::string getSatellite() {
		return getString(SatellitesManager::getInstance()->getSatelliteNames());
	}
	static std::string getString(const StringVector& strings);

	inline std::string getSelected() const {
		return widget.nameCombo->currentIndex() >= 0 ? widget.nameCombo->currentText().toStdString() : "";
	}

private:
	Ui::TLESelector widget;

	StringSelector(const StringVector& strings);
	virtual ~StringSelector();
};

#endif	/* _TLESELECTOR_H */
