#include "misc.h"

#include <cstdlib>



using namespace std;



int toInt(const std::string& value){
	return atoi(value.c_str());
}



long int toLong(const std::string& value){
	return atol(value.c_str());
}
