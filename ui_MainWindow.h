/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created: Wed Jul 24 20:56:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include "WorldMap.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *tleAdd;
    QAction *tleRemove;
    QAction *tleEdit;
    QAction *tleUpdate;
    QAction *satelliteRemove;
    QAction *satelliteEdit;
    QAction *satelliteAddExistingTLE;
    QAction *satelliteAddNewTLE;
    QWidget *centralwidget;
    WorldMap *map;
    QComboBox *tleCombo;
    QLabel *passLabel;
    QFrame *systemTimeFrame;
    QLabel *systemTimeCaptionLabel;
    QLabel *systemTimeLabel;
    QFrame *timeBaseFrame;
    QLabel *timeBaseCaptionLabel;
    QLabel *timeBaseLabel;
    QPushButton *connectButton;
    QLineEdit *rotatorAddressEdit;
    QDoubleSpinBox *fSpin;
    QLabel *fLabel;
    QMenuBar *menubar;
    QMenu *menuWatched;
    QMenu *menuAdd;
    QMenu *menuTLE;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(978, 660);
        MainWindow->setDocumentMode(false);
        tleAdd = new QAction(MainWindow);
        tleAdd->setObjectName(QString::fromUtf8("tleAdd"));
        tleRemove = new QAction(MainWindow);
        tleRemove->setObjectName(QString::fromUtf8("tleRemove"));
        tleEdit = new QAction(MainWindow);
        tleEdit->setObjectName(QString::fromUtf8("tleEdit"));
        tleUpdate = new QAction(MainWindow);
        tleUpdate->setObjectName(QString::fromUtf8("tleUpdate"));
        satelliteRemove = new QAction(MainWindow);
        satelliteRemove->setObjectName(QString::fromUtf8("satelliteRemove"));
        satelliteEdit = new QAction(MainWindow);
        satelliteEdit->setObjectName(QString::fromUtf8("satelliteEdit"));
        satelliteAddExistingTLE = new QAction(MainWindow);
        satelliteAddExistingTLE->setObjectName(QString::fromUtf8("satelliteAddExistingTLE"));
        satelliteAddNewTLE = new QAction(MainWindow);
        satelliteAddNewTLE->setObjectName(QString::fromUtf8("satelliteAddNewTLE"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        map = new WorldMap(centralwidget);
        map->setObjectName(QString::fromUtf8("map"));
        map->setGeometry(QRect(10, 10, 960, 480));
        tleCombo = new QComboBox(centralwidget);
        tleCombo->setObjectName(QString::fromUtf8("tleCombo"));
        tleCombo->setGeometry(QRect(10, 580, 201, 27));
        passLabel = new QLabel(centralwidget);
        passLabel->setObjectName(QString::fromUtf8("passLabel"));
        passLabel->setGeometry(QRect(430, 500, 541, 20));
        systemTimeFrame = new QFrame(centralwidget);
        systemTimeFrame->setObjectName(QString::fromUtf8("systemTimeFrame"));
        systemTimeFrame->setGeometry(QRect(10, 499, 201, 71));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        QBrush brush2(QColor(255, 255, 220, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
        systemTimeFrame->setPalette(palette);
        systemTimeFrame->setAutoFillBackground(true);
        systemTimeFrame->setFrameShape(QFrame::Box);
        systemTimeFrame->setFrameShadow(QFrame::Plain);
        systemTimeCaptionLabel = new QLabel(systemTimeFrame);
        systemTimeCaptionLabel->setObjectName(QString::fromUtf8("systemTimeCaptionLabel"));
        systemTimeCaptionLabel->setGeometry(QRect(5, 5, 191, 17));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(systemTimeCaptionLabel->sizePolicy().hasHeightForWidth());
        systemTimeCaptionLabel->setSizePolicy(sizePolicy);
        QPalette palette1;
        QBrush brush3(QColor(0, 255, 0, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        QBrush brush4(QColor(60, 60, 60, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        QBrush brush5(QColor(159, 158, 158, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush5);
        systemTimeCaptionLabel->setPalette(palette1);
        QFont font;
        font.setPointSize(8);
        systemTimeCaptionLabel->setFont(font);
        systemTimeCaptionLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        systemTimeLabel = new QLabel(systemTimeFrame);
        systemTimeLabel->setObjectName(QString::fromUtf8("systemTimeLabel"));
        systemTimeLabel->setGeometry(QRect(5, 20, 191, 41));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        systemTimeLabel->setPalette(palette2);
        QFont font1;
        font1.setPointSize(36);
        font1.setBold(false);
        font1.setWeight(50);
        systemTimeLabel->setFont(font1);
        systemTimeLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        timeBaseFrame = new QFrame(centralwidget);
        timeBaseFrame->setObjectName(QString::fromUtf8("timeBaseFrame"));
        timeBaseFrame->setGeometry(QRect(220, 500, 201, 71));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Dark, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Mid, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush);
        palette3.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        palette3.setBrush(QPalette::Active, QPalette::ToolTipBase, brush2);
        palette3.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Dark, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Mid, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Dark, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Mid, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
        timeBaseFrame->setPalette(palette3);
        timeBaseFrame->setAutoFillBackground(true);
        timeBaseFrame->setFrameShape(QFrame::Box);
        timeBaseFrame->setFrameShadow(QFrame::Plain);
        timeBaseCaptionLabel = new QLabel(timeBaseFrame);
        timeBaseCaptionLabel->setObjectName(QString::fromUtf8("timeBaseCaptionLabel"));
        timeBaseCaptionLabel->setGeometry(QRect(5, 5, 191, 17));
        sizePolicy.setHeightForWidth(timeBaseCaptionLabel->sizePolicy().hasHeightForWidth());
        timeBaseCaptionLabel->setSizePolicy(sizePolicy);
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush5);
        timeBaseCaptionLabel->setPalette(palette4);
        timeBaseCaptionLabel->setFont(font);
        timeBaseCaptionLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        timeBaseLabel = new QLabel(timeBaseFrame);
        timeBaseLabel->setObjectName(QString::fromUtf8("timeBaseLabel"));
        timeBaseLabel->setGeometry(QRect(5, 20, 191, 41));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        timeBaseLabel->setPalette(palette5);
        timeBaseLabel->setFont(font1);
        timeBaseLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        connectButton = new QPushButton(centralwidget);
        connectButton->setObjectName(QString::fromUtf8("connectButton"));
        connectButton->setGeometry(QRect(430, 500, 98, 27));
        rotatorAddressEdit = new QLineEdit(centralwidget);
        rotatorAddressEdit->setObjectName(QString::fromUtf8("rotatorAddressEdit"));
        rotatorAddressEdit->setGeometry(QRect(540, 500, 181, 27));
        fSpin = new QDoubleSpinBox(centralwidget);
        fSpin->setObjectName(QString::fromUtf8("fSpin"));
        fSpin->setGeometry(QRect(540, 530, 181, 27));
        fSpin->setDecimals(3);
        fSpin->setMinimum(10);
        fSpin->setMaximum(1000);
        fSpin->setValue(100);
        fLabel = new QLabel(centralwidget);
        fLabel->setObjectName(QString::fromUtf8("fLabel"));
        fLabel->setGeometry(QRect(430, 533, 66, 17));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 978, 25));
        menuWatched = new QMenu(menubar);
        menuWatched->setObjectName(QString::fromUtf8("menuWatched"));
        menuAdd = new QMenu(menuWatched);
        menuAdd->setObjectName(QString::fromUtf8("menuAdd"));
        menuTLE = new QMenu(menubar);
        menuTLE->setObjectName(QString::fromUtf8("menuTLE"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuWatched->menuAction());
        menubar->addAction(menuTLE->menuAction());
        menuWatched->addAction(menuAdd->menuAction());
        menuWatched->addAction(satelliteRemove);
        menuWatched->addAction(satelliteEdit);
        menuAdd->addAction(satelliteAddExistingTLE);
        menuAdd->addAction(satelliteAddNewTLE);
        menuTLE->addAction(tleAdd);
        menuTLE->addAction(tleRemove);
        menuTLE->addAction(tleEdit);
        menuTLE->addSeparator();
        menuTLE->addAction(tleUpdate);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "AntRot", 0, QApplication::UnicodeUTF8));
        tleAdd->setText(QApplication::translate("MainWindow", "Add...", 0, QApplication::UnicodeUTF8));
        tleRemove->setText(QApplication::translate("MainWindow", "Remove...", 0, QApplication::UnicodeUTF8));
        tleEdit->setText(QApplication::translate("MainWindow", "Edit...", 0, QApplication::UnicodeUTF8));
        tleUpdate->setText(QApplication::translate("MainWindow", "Update...", 0, QApplication::UnicodeUTF8));
        satelliteRemove->setText(QApplication::translate("MainWindow", "Remove...", 0, QApplication::UnicodeUTF8));
        satelliteEdit->setText(QApplication::translate("MainWindow", "Edit...", 0, QApplication::UnicodeUTF8));
        satelliteAddExistingTLE->setText(QApplication::translate("MainWindow", "Using existing TLE", 0, QApplication::UnicodeUTF8));
        satelliteAddNewTLE->setText(QApplication::translate("MainWindow", "Using new TLE", 0, QApplication::UnicodeUTF8));
        passLabel->setText(QString());
        systemTimeCaptionLabel->setText(QApplication::translate("MainWindow", "SYSTEM TIME", 0, QApplication::UnicodeUTF8));
        systemTimeLabel->setText(QApplication::translate("MainWindow", "88:88:88", 0, QApplication::UnicodeUTF8));
        timeBaseCaptionLabel->setText(QApplication::translate("MainWindow", "TIME BASE", 0, QApplication::UnicodeUTF8));
        timeBaseLabel->setText(QApplication::translate("MainWindow", "88:88:88", 0, QApplication::UnicodeUTF8));
        connectButton->setText(QApplication::translate("MainWindow", "Connect", 0, QApplication::UnicodeUTF8));
        rotatorAddressEdit->setText(QApplication::translate("MainWindow", "158.193.216.78:11111", 0, QApplication::UnicodeUTF8));
        fLabel->setText(QApplication::translate("MainWindow", "f [MHz]", 0, QApplication::UnicodeUTF8));
        menuWatched->setTitle(QApplication::translate("MainWindow", "Satellite", 0, QApplication::UnicodeUTF8));
        menuAdd->setTitle(QApplication::translate("MainWindow", "Add", 0, QApplication::UnicodeUTF8));
        menuTLE->setTitle(QApplication::translate("MainWindow", "TLE", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
