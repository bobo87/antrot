#include "TLEManager.h"



using namespace std;
using namespace pugi;



const std::string TLEManager::TLE_FILE = "./data/tle/tle.xml";



void TLEManager::update(const TLE& tle){
	if(tle.isValid())
		data[tle.getName()] = tle;
}



void TLEManager::update(TLEUpdateSource *source){
	source->addListener(this);
	source->update();
	source->removeListener(this);
}



StringVector TLEManager::getSatelliteNames() const{
	StringVector ret;
	for(TLEMap::const_iterator it=data.begin(); it!=data.end(); it++)
		ret.push_back((*it).first);
	return ret;
}



void TLEManager::addNode(pugi::xml_node root, const TLE& tle){
	xml_node node = root.append_child("tle");
	node.append_attribute("name").set_value(tle.getName().c_str());

	for(int i=1; i<=2; i++){
		xml_node line = node.append_child("line");
		line.append_attribute("number").set_value(i);
		line.append_child(pugi::node_pcdata).set_value(tle.getLine(i).c_str());
	}
}



void TLEManager::save(){
	xml_document doc;
	for(TLEMap::const_iterator it = data.begin(); it!=data.end(); it++)
		addNode(doc.root(), it->second);
	doc.save_file(TLE_FILE.c_str());
}



void TLEManager::load(){
	xml_document doc;
	xml_parse_result result = doc.load_file(TLE_FILE.c_str());
	if(result.status != status_ok)
		throw LoadingError("can't load '"+TLE_FILE+"', error : "+result.description());

	for (pugi::xml_node tleNode = doc.root().child("tle"); tleNode; tleNode = tleNode.next_sibling("tle")){
		string name = tleNode.attribute("name").as_string("");
		string line[2];
		for(int i=0; i<2; i++){
			 line[i] = tleNode.find_child_by_attribute("line", "number", toString(i+1).c_str()).child_value();
		}
		TLE tle(name, line[0], line[1]);
		if(!tle.isValid()){
			cout << "Invalid TLE" << endl;
			for(int i=0; i<3; i++)
				cout << "'" << tle.getLine(i) << "'" << endl;
			cout << "===========" << endl;
			continue;
		}
		data[tle.getName()] = tle;
	}
}
