#include "Rotator.h"

#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstdlib>



USING_PTYPES;
using namespace std;



void Rotator::connect(const std::string& address){
	size_t portSeparator = address.find(':');

	if(portSeparator == std::string::npos){
		throw RotatorError("can't find port separator character ':'");
		return;
	}

	try{
		stream.set_host(address.substr(0, portSeparator).c_str());
		stream.set_port(atoi(address.substr(portSeparator+1).c_str()));
		stream.open();
	}catch(estream* e){
		cout << "problem opening stream : " << (const char*)e->get_message() << endl;
		throw RotatorError((const char*)e->get_message());
		return;
	}
	cout << "connected to rotator at " << address << endl;

	try{
		stream.put("ROTATOR>\r");
		stream.flush();
	}catch(estream* e){
		cout << "problem setting rotator mode : " << (const char*)e->get_message() << endl;
		stream.close();
		throw RotatorError((const char*)e->get_message());
		return;
	}
	cout << "rotator mode set OK" << endl;

	connected = true;
}



void Rotator::disconnect(){
	if(!connected)
		return;

	cleanInput();
	try{
		stream.close();
	}catch(estream* e){
		cout << "problem closing stream : " << (const char*)e->get_message() << endl;
	}
	connected = false;
	cout << "rotator disconnected" << endl;
}



void Rotator::pointTo(double azimuth, double elevation){
	cleanInput();

	char buffer[64];
	sprintf(buffer, "A%.1fE%.1f'", azimuth/(M_PI/180.0f), elevation/(M_PI/180.0f));

	cout << "Rotator::pointTo(" << azimuth << ", " << elevation << ") ---> \"" << buffer << "\"";
	if(!connected){
		cout << " - not connected" << endl;
		return;
	}
	cout << endl;

	try{
		stream.putf("%s\r", buffer);
		stream.flush();
	}catch(estream* e){
		cout << "problem sending command : " << (const char*)e->get_message() << endl;
	}
}



void Rotator::cleanInput(){
	if(!connected)
		return;
	try{
		stream.clear();
//		while(!stream.get_eof()){
//			const char* RESPONSE = stream.line();
//			cout << "ROTATOR ---> " << RESPONSE << endl;
//		}
	}catch(estream* e){
		cout << "problem cleaning input : " << (const char*)e->get_message() << endl;
	}
}



std::string Rotator::getLastPosition(){
	try{
		cleanInput();
		stream.put("POSMEM?\r");
		stream.flush();
		return (const char*)stream.line();
	}catch(estream* e){
		cout << "problem getting lastposition : " << (const char*)e->get_message() << endl;
	}
	return "ERROR";
}
