/*
 * File:   FrequencyPair.h
 * Author: peter
 *
 * Created on October 10, 2012, 10:41 PM
 */

#ifndef FREQUENCYPAIR_H
#define	FREQUENCYPAIR_H



typedef unsigned long Frequency;



class FrequencyPair{
public:
	/**
	 * computed frequencies for downlink/uplink data
	 *
     * @param downlink downlink frequency in Hz
     * @param uplink uplink frequency in Hz
     */
	FrequencyPair(Frequency downlink = 0, Frequency uplink = 0) :
		downlink(downlink), uplink(uplink) { }

	inline Frequency getDownlink() const { return downlink; }
	inline Frequency getUplink() const { return uplink; }
private:
	Frequency downlink, uplink;
};



#endif	/* FREQUENCYPAIR_H */
