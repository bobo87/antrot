/*
 * File:   TLEFileUpdate.h
 * Author: peter
 *
 * Created on October 20, 2012, 4:53 PM
 */

#ifndef TLEFILEUPDATE_H
#define	TLEFILEUPDATE_H



#include "TLEUpdateSource.h"
#include "FileDownloader.h"



class TLEFileUpdate : public TLEUpdateSource{
public:
	TLEFileUpdate(const std::string& url);

	void update();
	
private:
	FileDownloader downloader;
};



#endif	/* TLEFILEUPDATE_H */
