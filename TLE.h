/*
 * File:   TLE.h
 * Author: peter
 *
 * Created on October 10, 2012, 10:58 PM
 */

#ifndef TLE_H
#define	TLE_H



#include <string>
#include <cassert>



class Satellite;



class TLE{
public:
	TLE(const std::string& name, const std::string& line1, const std::string& line2);

	inline TLE() :
		empty(true), name(""), satellite(0) { line[0] = line[1] = ""; }

	inline std::string getName() const { return name; }
	inline std::string getLine(int index) const { assert(index >= 0 && index < 3); return index ? line[index-1] : name; }

	inline Satellite* getSatellite() const { return satellite; }
	inline void setSatellite(Satellite *satellite) { this->satellite = satellite; }
	
	bool isValid() const;
	inline bool isEmpty() const {
		return empty;
	}

private:
	bool empty;

	std::string name, line[2];

	Satellite *satellite;

	static bool isFloat(const std::string& str);
	static bool isInteger(const std::string& str);
	static bool isAlnum(const std::string& str);
};


#endif	/* TLE_H */

