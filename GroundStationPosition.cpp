#include "GroundStationPosition.h"

#include "GroundStationPositionListener.h"



GroundStationPosition GroundStationPosition::position(0.0, 0.0, 0.0);
GroundStationPosition::ListenersSet GroundStationPosition::listeners;



void GroundStationPosition::addListener(GroundStationPositionListener* listener, bool fireNow){
	listeners.insert(listener);
	if(fireNow)
		listener->onGroundStationPositionChanged(position);
}



void GroundStationPosition::setPosition(GroundStationPosition position){
	GroundStationPosition::position = position;
	notifyAll();
}



void GroundStationPosition::notifyAll(){
	for(ListenersSet::iterator it=listeners.begin(); it!=listeners.end(); it++)
		(*it)->onGroundStationPositionChanged(position);
}
