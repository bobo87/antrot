#include "Satellite.h"

#include "TimeBase.h"



Satellite::Satellite(const TLE& tle, int priority) :
	tle(tle),
	predictor(0),
	lastPosition(0),
	priority(priority){
}



SatellitePosition Satellite::getPosition(std::time_t time){
	QMutexLocker locker(&tleMutex);

	if(!predictor)
		predictor = new SatellitePredictor(GroundStationPosition::getPosition(), tle);

	if(!time)
		time = TimeBase::getInstance()->getTime();

	predictor->setTime(time);
	if(!lastPosition)
		return *(lastPosition = new SatellitePosition(predictor->getPosition()));
	else
		return (*lastPosition) = predictor->getPosition();
}



TimeSlot Satellite::getNextPass(std::time_t time){
	QMutexLocker locker(&tleMutex);

	if(!predictor)
		predictor = new SatellitePredictor(GroundStationPosition::getPosition(), tle);

	predictor->setTime(time);
	return predictor->getNextPass();
}



FrequencyPair Satellite::getDopplerFrequencies(std::time_t time){
	QMutexLocker locker(&tleMutex);

	if(!predictor)
		predictor = new SatellitePredictor(GroundStationPosition::getPosition(), tle);

	predictor->setTime(time);

	return FrequencyPair(
		predictor->getFrequencyPair(frequency.getDownlink()).getDownlink(),
		predictor->getFrequencyPair(frequency.getUplink()).getUplink());
}



SatellitePosition Satellite::getLastPosition(){
	return lastPosition ? *lastPosition : *(lastPosition = new SatellitePosition(getPosition()));
}



void Satellite::onGroundStationPositionChanged(GroundStationPosition newPosition){
	if(predictor){
		delete predictor;
		predictor = new SatellitePredictor(newPosition, tle);
	}
}
