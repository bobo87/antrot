/*
 * File:   TLEUpdateSource.h
 * Author: peter
 *
 * Created on October 20, 2012, 1:28 PM
 */

#ifndef TLEUPDATESOURCE_H
#define	TLEUPDATESOURCE_H



#include "TLEUpdateListener.h"

#include <stdexcept>
#include <set>



class TLEUpdateSource{
public:
	virtual ~TLEUpdateSource() { }

	virtual void update() = 0;

	inline void addListener(TLEUpdateListener* listener){
		listeners.insert(listener);
	}

	inline void removeListener(TLEUpdateListener* listener){
		listeners.erase(listener);
	}

	class UpdateError : public std::runtime_error{
	public:
		UpdateError(const std::string& error) :
			std::runtime_error(error.c_str()) { }
	};

protected:
	void update(const TLE& tle);

private:
	typedef std::set<TLEUpdateListener*> Listeners;
	Listeners listeners;
};


#endif	/* TLEUPDATESOURCE_H */

