#include "TLEFileUpdate.h"

#include "misc.h"

#include <iostream>
#include <sstream>



using namespace std;



TLEFileUpdate::TLEFileUpdate(const std::string& url) :
	downloader(url) {
}



void TLEFileUpdate::update(){
	string data;

	try{
		data = downloader.download();
	} catch (FileDownloader::DownloadError& err){
		throw UpdateError(err.what());
	}

	istringstream stream(data);
	bool valid = true, eof = false;
	while(!stream.eof()){
		string line[3];
		for(int i=0; i<3; i++)
			if(stream.eof()){
				eof = true;
				break;
			}else{
				getline(stream, line[i]);
				trim(line[i]);
			}
		if(eof)
			break;

		const TLE NEW_TLE = TLE(line[0], line[1], line[2]);
		if(!NEW_TLE.isValid()){
			valid        = false;
			cout << "INVALID TLE" << endl;
			for(int i=0; i<3; i++)
				cout << "'" << NEW_TLE.getLine(i) << "'" << endl;
			continue;
		}

		TLEUpdateSource::update(NEW_TLE);
	}

	if(!valid)
		throw UpdateError("update file contain invalid record(s)");
}
