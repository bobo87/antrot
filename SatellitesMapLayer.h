/*
 * File:   SatellitesMapLayer.h
 * Author: peter
 *
 * Created on October 13, 2012, 8:09 PM
 */

#ifndef SATELLITESMAPLAYER_H
#define	SATELLITESMAPLAYER_H



#include "MapLayer.h"
#include "SatellitePosition.h"
#include "misc.h"

#include <QPixmap>



class SatellitesMapLayer : public MapLayer{
public:
	SatellitesMapLayer(WorldMap *map) :
		MapLayer(map), satellites(0), icon("./data/gfx/sat16.png") { }

	void render(QPainter& painter);

	inline void setSatellitesVector(SatellitesVector *satellites) { this->satellites = satellites; }
private:
	SatellitesVector* satellites;
	QPixmap icon;
};



#endif	/* SATELLITESMAPLAYER_H */

