/*
 * File:   GroundStationPositionListener.h
 * Author: peter
 *
 * Created on July 5, 2013, 8:57 PM
 */

#ifndef GROUNDSTATIONPOSITIONLISTENER_H
#define	GROUNDSTATIONPOSITIONLISTENER_H



#include "GroundStationPosition.h"



class GroundStationPositionListener{
public:
	inline GroundStationPositionListener(){
		GroundStationPosition::addListener(this);
	}

	inline virtual ~GroundStationPositionListener() {
		GroundStationPosition::removeListener(this);
	}

	virtual void onGroundStationPositionChanged(GroundStationPosition newPosition) = 0;
};


#endif	/* GROUNDSTATIONPOSITIONLISTENER_H */
