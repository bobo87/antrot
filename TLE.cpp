#include "TLE.h"

#include "misc.h"

#include <iostream>
#include <sstream>
#include <cstdlib>



using namespace std;



TLE::TLE(const std::string& name, const std::string& line1, const std::string& line2) :
	empty(false), name(name), satellite(0) {
	line[0] = line1;
	line[1] = line2;

	line[0]    = trim(line[0]);
	line[1]    = trim(line[1]);
	this->name = trim(this->name);
}



bool TLE::isValid() const{
	if(name.length()<1 || name.length()>69 || line[0].length()!=69 || line[1].length()!=69)
		return false;

	const string&
		L1 = line[0],
		L2 = line[1];

	return
		L1[0]=='1' &&
		isInteger(L1.substr(2, 5)) &&
		isalpha(L1[7]) &&
		isInteger(L1.substr(9, 5)) &&
		isAlnum(L1.substr(14, 3)) &&
		isInteger(L1.substr(18, 5)) &&
		L1[23]=='.' &&
		isInteger(L1.substr(24, 8)) &&
		L1[34]=='.' &&
		isInteger(L1.substr(35, 8)) &&
		isInteger(L1.substr(44, 5)) &&
		(L1[50]=='-' || L1[50]=='+') &&
		isdigit(L1[51]) &&
		isInteger(L1.substr(53, 5)) &&
		(L1[59]=='-' || L1[59]=='+') &&
		isdigit(L1[60]) &&
		isdigit(L1[62]) &&
		isInteger(L1.substr(64, 4)) &&
		isdigit(L1[68]) &&

		L2[0]=='2' &&
		isInteger(L2.substr(2, 5)) &&
		isInteger(L2.substr(8, 3)) &&
		L2[11]=='.' &&
		isInteger(L2.substr(12, 4)) &&
		isInteger(L2.substr(17, 3)) &&
		L2[20]=='.' &&
		isInteger(L2.substr(21, 4)) &&
		isInteger(L2.substr(26, 7)) &&
		isInteger(L2.substr(34, 3)) &&
		L2[37]=='.' &&
		isInteger(L2.substr(38, 4)) &&
		isInteger(L2.substr(43, 3)) &&
		L2[46]=='.' &&
		isInteger(L2.substr(47, 4)) &&
		isInteger(L2.substr(52, 2)) &&
		L2[54]=='.' &&
		isInteger(L2.substr(55, 8)) &&
		isInteger(L2.substr(63, 5)) &&
		isdigit(L2[68]);
}



bool TLE::isFloat(const std::string& str) {
    istringstream iss(str);
    float dummy;
    iss >> noskipws >> dummy;
    return iss && iss.eof();
}



bool TLE::isInteger(const std::string& str){
	const int LENGTH = str.length();
	if(LENGTH==1)
		return isdigit(str[0]);

	//TODO better '-' check, can be only as first character
	for(int i=0; i<LENGTH; i++)
		if(!(isdigit(str[i]) || isspace(str[i]) || str[i]=='-'))
			return false;
	return true;
}



bool TLE::isAlnum(const std::string& str){
	const int LENGTH = str.length();

	for(int i=0; i<LENGTH; i++)
		if(!(isalnum(str[i]) || isspace(str[i])))
			return false;
	return true;
}
