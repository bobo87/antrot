/*
 * File:   DaylightMapLayer.h
 * Author: peter
 *
 * Created on October 27, 2012, 10:45 PM
 */

#ifndef DAYLIGHTMAPLAYER_H
#define	DAYLIGHTMAPLAYER_H



#include "DaylightMaskGenerator.h"
#include "MapLayer.h"
#include "TimeBase.h"



class DaylightMapLayer : public MapLayer{
public:
	DaylightMapLayer(WorldMap *map);

	inline void resize(QResizeEvent* event){
		generator.setSize(event->size().width(), event->size().height());
	}

	void render(QPainter& painter);

private:
	DaylightMaskGenerator generator;
	UnixTime deadTime, lastRender;
};



#endif	/* DAYLIGHTMAPLAYER_H */
