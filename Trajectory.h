/*
 * File:   Trajectory.h
 * Author: peter
 *
 * Created on October 14, 2012, 12:45 AM
 */

#ifndef TRAJECTORY_H
#define	TRAJECTORY_H



#include "SatellitePosition.h"

#include <vector>



class Trajectory{
public:
	Trajectory(std::size_t maxPoints = 1200) : maxPoints(maxPoints) { }

	void recordPoint(SatellitePosition position);

	int size() const { return trajectory.size(); }
	SatellitePosition operator[](int index) const { return trajectory[index]; }

private:
	std::vector<SatellitePosition> trajectory;
	std::size_t maxPoints;
};


#endif	/* TRAJECTORY_H */

