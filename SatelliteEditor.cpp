/*
 * File:   SatelliteEditor.cpp
 * Author: peter
 *
 * Created on January 13, 2013, 10:29 PM
 */

#include "SatelliteEditor.h"



SatelliteEditor::SatelliteEditor(Satellite *satellite)
	:satellite(satellite){
	widget.setupUi(this);

	if(!satellite)
		return;
	widget.prioritySpin->setValue(satellite->getPriority());
	widget.uploadSpin->setValue(satellite->getFrequencyPair().getUplink() / 1e6);
	widget.downloadSpin->setValue(satellite->getFrequencyPair().getDownlink() / 1e6);

	connect(this, SIGNAL(accepted()), this, SLOT(accepted()));
}



SatelliteEditor::~SatelliteEditor(){
}



void SatelliteEditor::accepted(){
	satellite->setPriority(widget.prioritySpin->value());
	satellite->setFrequencyPair(FrequencyPair(
		(Frequency)(widget.downloadSpin->value()*1e6),
		(Frequency)(widget.uploadSpin->value()*1e6)
		));
}
