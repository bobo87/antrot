/*
 * File:   MapLayer.h
 * Author: peter
 *
 * Created on October 13, 2012, 5:33 PM
 */

#ifndef MAPLAYER_H
#define	MAPLAYER_H



#include "WorldMap.h"

#include <QResizeEvent>
#include <QPainter>
#include <cmath>



class MapLayer{
public:
	MapLayer(WorldMap *map) : map(map) { }
	~MapLayer() { }

	virtual void render(QPainter& painter) = 0;

	virtual void resize(QResizeEvent*) { }

	inline WorldMap* getMap() const { return map; }

	inline int longitudeToPixel(float longitude) const {
		return longitudeToPixel(longitude, map->width());
	}
	inline int latitudeToPixel(float latitude) const {
		return latitudeToPixel(latitude, map->height());
	}
	inline float pixelToLongitude(int x) const {
		return pixelToLongitude(x, map->width());
	}
	inline float pixelToLatitude(int y) const {
		return pixelToLatitude(y, map->height());
	}

	static int longitudeToPixel(float longitude, int width);
	static int latitudeToPixel(float latitude, int height);
	static float pixelToLongitude(int x, int width);
	static float pixelToLatitude(int y, int height);

private:
	WorldMap *map;
};


#endif	/* MAPLAYER_H */

