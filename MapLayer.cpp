#include "MapLayer.h"



int MapLayer::longitudeToPixel(float longitude, int width){
	if(longitude > M_PI)
		longitude -= 2.0f*M_PI;
	return (int)(width*(fmod(longitude, M_PI)+M_PI)/(2.0f*M_PI));
}



int MapLayer::latitudeToPixel(float latitude, int height){
	if(latitude > M_PI/2.0f)
		latitude -= M_PI;
	return (int)(height*(fmod(-latitude, M_PI/2)+M_PI/2.0f)/M_PI);
}



float MapLayer::pixelToLongitude(int x, int width){
	return M_PI*(2.0f*(float)x/(float)width-1.0f);
}



float MapLayer::pixelToLatitude(int y, int height){
	return M_PI*(0.5f-(float)y/(float)height);
}
