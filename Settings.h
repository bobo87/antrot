/*
 * File:   Settings.h
 * Author: peter
 *
 * Created on December 14, 2012, 9:49 PM
 */

#ifndef SETTINGS_H
#define	SETTINGS_H



#include "Singleton.h"
#include "TimeBase.h"

#include <string>



class Settings : public Singleton<Settings>{
public:
	inline long getMapRedrawInterval() const { return mapRedrawInterval; }

	inline long getTrackingInterval() const { return trackingInterval; }

	inline UnixTime getSaveMapInterval() const { return saveMapInterval; }
	inline std::string getMapFileName() const { return mapFileName; }
	inline std::string getRadarFileName() const { return radarFileName; }

private:
	Settings();
	friend class Singleton<Settings>;

	long mapRedrawInterval;

	long trackingInterval;

	UnixTime saveMapInterval;
	std::string mapFileName;
	std::string radarFileName;
};



#endif	/* SETTINGS_H */
