#include "TimeBase.h"



using namespace std;



UnixTime TimeBase::getTime(){
	return time(0) + delta;
}



UnixTime TimeBase::getSystemTime(){
	return time(0);
}



std::string timeToString(UnixTime unixTime){
	string ret = asctime(localtime(&unixTime));
	return ret.substr(0, ret.length()-1);
}
