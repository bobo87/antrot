/*
 * File:   FileDownloader.h
 * Author: peter
 *
 * Created on October 19, 2012, 11:17 PM
 */

#ifndef FILEDOWNLOADER_H
#define	FILEDOWNLOADER_H



#include <curl/curl.h>
#include <curl/easy.h>
#include <stdexcept>
#include <string>
#include <fstream>



class FileDownloader{
public:
	FileDownloader(const std::string& url, const std::string& destFile="");
	~FileDownloader();

	std::string download();

	inline static std::string download(const std::string& url, const std::string& destFile=""){
		FileDownloader downloader(url, destFile);
		return downloader.download();
	}



	class DownloadError : public std::runtime_error{
	public:
		DownloadError(const std::string& error) :
			std::runtime_error(error.c_str()) { }
	};

private:
	CURL *curl;
	std::string url;

	std::string data;
	std::ofstream *file;

	struct OutputStream{
		std::string *fileString;
		std::ofstream *file;
		OutputStream() :
		fileString(0),
		file(0) { }
	};
	OutputStream outputStream;

	static size_t writeCallback(void *data, size_t size, size_t nmemb, OutputStream *stream);


	class CurlLifecycle{
	public:
		inline CurlLifecycle() { curl_global_init(CURL_GLOBAL_ALL); }
		inline ~CurlLifecycle() { curl_global_cleanup(); }
	};
	static CurlLifecycle lifecycle;
};



#endif	/* FILEDOWNLOADER_H */
