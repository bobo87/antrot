/********************************************************************************
** Form generated from reading UI file 'TLESelector.ui'
**
** Created: Wed Jul 24 20:56:55 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TLESELECTOR_H
#define UI_TLESELECTOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_TLESelector
{
public:
    QDialogButtonBox *buttonBox;
    QComboBox *nameCombo;
    QLabel *nameLabel;

    void setupUi(QDialog *TLESelector)
    {
        if (TLESelector->objectName().isEmpty())
            TLESelector->setObjectName(QString::fromUtf8("TLESelector"));
        TLESelector->setWindowModality(Qt::ApplicationModal);
        TLESelector->resize(269, 77);
        buttonBox = new QDialogButtonBox(TLESelector);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 40, 251, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);
        nameCombo = new QComboBox(TLESelector);
        nameCombo->setObjectName(QString::fromUtf8("nameCombo"));
        nameCombo->setGeometry(QRect(70, 10, 191, 27));
        nameLabel = new QLabel(TLESelector);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));
        nameLabel->setGeometry(QRect(10, 13, 66, 17));

        retranslateUi(TLESelector);
        QObject::connect(buttonBox, SIGNAL(accepted()), TLESelector, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TLESelector, SLOT(reject()));

        QMetaObject::connectSlotsByName(TLESelector);
    } // setupUi

    void retranslateUi(QDialog *TLESelector)
    {
        TLESelector->setWindowTitle(QApplication::translate("TLESelector", "Select name", 0, QApplication::UnicodeUTF8));
        nameLabel->setText(QApplication::translate("TLESelector", "Name", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TLESelector: public Ui_TLESelector {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TLESELECTOR_H
