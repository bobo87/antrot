/*
 * File:   MainWindow.cpp
 * Author: peter
 *
 * Created on October 12, 2012, 10:33 PM
 */

#include "MainWindow.h"

#include "TLEUpdateForm.h"
#include "SatellitesManager.h"
#include "StringSelector.h"
#include "Rotator.h"
#include "TimeBase.h"
#include "TLEManager.h"
#include "FileDownloader.h"
#include "SatellitePredictor.h"
#include "misc.h"
#include "TLEFileUpdate.h"
#include "TLEEditForm.h"
#include "SatelliteEditor.h"

#include <QMessageBox>
#include <algorithm>
#include <QTimer>
#include <cstdio>
#include <cmath>
#include <ctime>



using namespace std;



MainWindow::MainWindow() :
	tracker(new Tracker()){
	assert(tracker);

	GroundStationPosition::setPosition(GroundStationPosition(48.9984f, 21.2339f, 250.0f));

	widget.setupUi(this);

	TLEManager *manager = TLEManager::getInstance();
//	const std::string SOURCES[] = {
//		"http://www.celestrak.com/NORAD/elements/stations.txt",
//		"http://www.celestrak.com/NORAD/elements/visual.txt",
//		"http://www.celestrak.com/NORAD/elements/weather.txt",
//		"http://www.celestrak.com/NORAD/elements/noaa.txt",
//		"http://www.celestrak.com/NORAD/elements/goes.txt",
//		"http://www.celestrak.com/NORAD/elements/resource.txt",
//		"http://www.celestrak.com/NORAD/elements/sarsat.txt",
//		"http://www.celestrak.com/NORAD/elements/dmc.txt",
//		"http://www.celestrak.com/NORAD/elements/tdrss.txt",
//		"http://www.celestrak.com/NORAD/elements/geo.txt",
//		"http://www.celestrak.com/NORAD/elements/intelsat.txt",
//		"http://www.celestrak.com/NORAD/elements/gorizont.txt",
//		"http://www.celestrak.com/NORAD/elements/raduga.txt",
//		"http://www.celestrak.com/NORAD/elements/molniya.txt",
//		"http://www.celestrak.com/NORAD/elements/iridium.txt",
//		"http://www.celestrak.com/NORAD/elements/orbcomm.txt",
//		"http://www.celestrak.com/NORAD/elements/globalstar.txt",
//		"http://www.celestrak.com/NORAD/elements/amateur.txt",
//		"http://www.celestrak.com/NORAD/elements/x-comm.txt",
//		"http://www.celestrak.com/NORAD/elements/other-comm.txt",
//		"http://www.celestrak.com/NORAD/elements/gps-ops.txt",
//		"http://www.celestrak.com/NORAD/elements/glo-ops.txt",
//		"http://www.celestrak.com/NORAD/elements/galileo.txt",
//		"http://www.celestrak.com/NORAD/elements/sbas.txt",
//		"http://www.celestrak.com/NORAD/elements/nnss.txt",
//		"http://www.celestrak.com/NORAD/elements/musson.txt",
//		"http://www.celestrak.com/NORAD/elements/science.txt",
//		"http://www.celestrak.com/NORAD/elements/geodetic.txt",
//		"http://www.celestrak.com/NORAD/elements/engineering.txt",
//		"http://www.celestrak.com/NORAD/elements/education.txt",
//		"http://www.celestrak.com/NORAD/elements/military.txt",
//		"http://www.celestrak.com/NORAD/elements/radar.txt",
//		"http://www.celestrak.com/NORAD/elements/cubesat.txt",
//		"http://www.celestrak.com/NORAD/elements/other.txt",
//		""
//	};
//	for(int i=0; SOURCES[i]!=""; i++){
//		printf("updating '%s' ... ", SOURCES[i].c_str());
//		TLEFileUpdate source(SOURCES[i]);
//		try{
//			manager->update(&source);
//		} catch(TLEUpdateSource::UpdateError& error){
//			printf("error, %s\n", error.what());
//			continue;
//		}
//		printf("ok\n");
//	}

//	manager->save();
	manager->load();

	SatellitesManager::getInstance()->load();

	StringVector names = manager->getSatelliteNames();
	sort(names.begin(), names.end());
	printf("List contains %d TLEs\n", names.size());
	for(StringVector::const_iterator it=names.begin(); it!=names.end(); it++)
		widget.tleCombo->addItem((*it).c_str());
//		printf("%s\n", (*it).c_str());

	satellites.push_back(new Satellite(manager->getTLE("ISS (ZARYA)")));

//	widget.map->setGroundStationPosition(&station);
	widget.map->setSatellitesVector(&satellites);

	QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(mapRedrawTimer()));
    timer->start(Settings::getInstance()->getMapRedrawInterval());

	connect(widget.tleCombo , SIGNAL(currentIndexChanged(int)), this, SLOT(tleChanged(int)));

	GroundStationPosition::notifyAll();

	tracker->start();
	tracker->setTrackedSatellite(satellites[0]);

	connect(widget.connectButton, SIGNAL(released()), this, SLOT(connectButton()));

	connect(widget.tleAdd, SIGNAL(triggered()), this, SLOT(tleAdd()));
	connect(widget.tleEdit, SIGNAL(triggered()), this, SLOT(tleEdit()));
	connect(widget.tleRemove, SIGNAL(triggered()), this, SLOT(tleRemove()));
	connect(widget.tleUpdate, SIGNAL(triggered()), this, SLOT(tleUpdate()));

	connect(widget.satelliteAddNewTLE, SIGNAL(triggered()), this, SLOT(satelliteAddNew()));
	connect(widget.satelliteAddExistingTLE, SIGNAL(triggered()), this, SLOT(satelliteAddExisting()));
	connect(widget.satelliteEdit, SIGNAL(triggered()), this, SLOT(satelliteEdit()));
	connect(widget.satelliteRemove, SIGNAL(triggered()), this, SLOT(satelliteRemove()));

	processMenu();
}



void MainWindow::processMenu(){
	bool tleCount = TLEManager::getInstance()->getCount() > 0;
	widget.tleRemove->setEnabled(tleCount);
	widget.tleEdit->setEnabled(tleCount);

	bool satCount = SatellitesManager::getInstance()->size() > 0;
	widget.satelliteRemove->setEnabled(satCount);
	widget.satelliteEdit->setEnabled(satCount);
}



MainWindow::~MainWindow() {
	TLEManager::getInstance()->save();

	if(tracker){
		tracker->quit();
		while(tracker->isRunning())
			;
		safeDelete(tracker);
	}

	Rotator::getInstance()->disconnect();
}



std::string unixToTime(std::time_t unixTime){
	string ret = asctime(localtime(&unixTime));
	return ret.substr(0, ret.length()-1);
}



void MainWindow::mapRedrawTimer(){
	const int MAX_BUF = 32;
	char buffer[MAX_BUF];

	UnixTime dispTime = TimeBase::getInstance()->getSystemTime();
	strftime(buffer, MAX_BUF-1, "%H:%M:%S", localtime(&dispTime));
	widget.systemTimeLabel->setText(buffer);
	dispTime = TimeBase::getInstance()->getTime();
	strftime(buffer, MAX_BUF-1, "%H:%M:%S", localtime(&dispTime));
	widget.timeBaseLabel->setText(buffer);


//	for(size_t i=0; i<satellites.size(); i++){
//		satellites[i]->getPosition(TimeBase::getInstance()->getTime());
//		satellites[i]->recordLastPosition();
//	}
	for(size_t i=0; i<SatellitesManager::getInstance()->size(); i++){
		Satellite* satellite = SatellitesManager::getInstance()->getSatellite(i);
		satellite->getPosition(TimeBase::getInstance()->getTime());
		satellite->recordLastPosition();
	}

	repaint();

//	cout << "LAST POSITION = \"" << Rotator::getInstance()->getLastPosition() << endl;
}



void MainWindow::tleChanged(int index){
	if(index<0)
		return;
	delete satellites[0];
	satellites[0] = new Satellite(TLEManager::getInstance()->getTLE(widget.tleCombo->currentText().toAscii().data()));
	tracker->setTrackedSatellite(satellites[0]);
}



void MainWindow::connectButton(){
	Rotator *rotator = Rotator::getInstance();

	try{
		if(rotator->isConnected())
			rotator->disconnect();
		else
			rotator->connect(widget.rotatorAddressEdit->text().toStdString());
	}catch(Rotator::RotatorError error){
		QMessageBox::critical(this, "Error", error.what());
	}

	if(rotator->isConnected()){
		widget.connectButton->setText("Disconnect");
		widget.rotatorAddressEdit->setEnabled(false);
	}else{
		widget.connectButton->setText("Connect");
		widget.rotatorAddressEdit->setEnabled(true);
	}
}



void MainWindow::tleAdd(){
	TLEEditForm *editor = new TLEEditForm();

	if(editor->exec() == QDialog::Rejected){
		delete editor;
		return;
	}

	processMenu();
	TLEManager::getInstance()->update(editor);
	delete editor;
}



void MainWindow::tleEdit(){
	string originalName = StringSelector::getTLE();
	if(originalName == "")
		return;

	TLEManager *manager = TLEManager::getInstance();

	TLEEditForm *editor = new TLEEditForm(manager->getTLE(originalName));
	if(editor->exec() == QDialog::Rejected){
		delete editor;
		return;
	}

	TLE tle = editor->getTLE();

	manager->update(tle);
	if(tle.getName() != originalName)
		manager->remove(originalName);

	if(tle.getSatellite())
		tle.getSatellite()->updateTLE(tle);

	delete editor;
}



void MainWindow::tleRemove(){
	string name = StringSelector::getTLE();
	if(name == "")
		return;

	TLEManager *manager = TLEManager::getInstance();

	TLE tle = manager->getTLE(name);
	manager->remove(name);

	if(tle.getSatellite()){
		;// delete satellite
	}

	if(tracker && tracker->getTrackedSatellite()->getTLE().getName() == name)
		tracker->setTrackedSatellite(0);

	processMenu();
}



void MainWindow::tleUpdate(){
	TLEUpdateForm updateForm;
	updateForm.exec();
}



void MainWindow::satelliteAdd(TLE& tle){
	if(SatellitesManager::getInstance()->exist(tle.getName())){
		QMessageBox::warning(this, "Satellite exist", "Can't create already existing satellite");
		return;
	}

	Satellite *satellite = new Satellite(tle);
	SatelliteEditor editor(satellite);
	if(editor.exec() == QDialog::Rejected)
		return;

	SatellitesManager::getInstance()->add(satellite);
	tle.setSatellite(satellite);
	TLEManager::getInstance()->update(tle);
	processMenu();
	SatellitesManager::getInstance()->save();
}



void MainWindow::satelliteAddNew(){
	TLEEditForm *editor = new TLEEditForm();

	if(editor->exec() == QDialog::Rejected){
		delete editor;
		return;
	}

	TLEManager::getInstance()->update(editor);
	TLE tle = editor->getTLE();
	delete editor;

	satelliteAdd(tle);
}



void MainWindow::satelliteAddExisting(){
	string name = StringSelector::getTLE();
	if(name == "")
		return;

	TLEManager *manager = TLEManager::getInstance();
	TLE tle = manager->getTLE(name);

	satelliteAdd(tle);
}



void MainWindow::satelliteEdit(){
	string originalName = StringSelector::getSatellite();
	if(originalName == "")
		return;
	TLEManager *manager = TLEManager::getInstance();

	Satellite *satellite = manager->getTLE(originalName).getSatellite();
	if(!satellite){
		QMessageBox::warning(this, "Editor failure", "Satellite not found!");
		return;
	}
	cout << "EDIT SATELLITE" << endl
		<< satellite->getTLE().getName() << endl
		<< satellite->getPriority() << endl
		<< satellite->getFrequencyPair().getDownlink() << ", " << satellite->getFrequencyPair().getUplink() << endl;
	SatelliteEditor editor(satellite);

	if(editor.exec() == QDialog::Rejected)
		return;

	SatellitesManager::getInstance()->save();
}



void MainWindow::satelliteRemove(){
	string name = StringSelector::getSatellite();
	if(name == "")
		return;

	SatellitesManager *manager = SatellitesManager::getInstance();

	int satIndex = manager->satelliteToIndex(name);

	TLE tle = TLEManager::getInstance()->getTLE(name);
	tle.setSatellite(0);
	TLEManager::getInstance()->update(tle);
	manager->remove(manager->getSatellite(satIndex));

	//TODO kontrola, ci sa satelit nepouziva

	manager->save();

	processMenu();
}
