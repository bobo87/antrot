#include "DaylightMaskGenerator.h"

#include "MapLayer.h"
#include "misc.h"

#include <QRadialGradient>
#include <QPainter>
#include <QBrush>
#include <cmath>
#include <iostream>



using namespace std;



QPixmap
	*DaylightMaskGenerator::sun      = 0,
	*DaylightMaskGenerator::nightMap = 0;



DaylightMaskGenerator::DaylightMaskGenerator() :
	result(0),
	resizedMap(0){
}



DaylightMaskGenerator::~DaylightMaskGenerator(){
	safeDelete(result);
	safeDelete(resizedMap);
}



void DaylightMaskGenerator::setSize(int width, int height){
	if(result && (result->width()==width && result->height()==height))
		return;

	safeDelete(resizedMap);
	resizedMap = new QPixmap(nightMap->scaled(width, height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));

	safeDelete(result);
	result = new QPixmap(width, height);
	result->fill(QColor(Qt::transparent));
}



void DaylightMaskGenerator::loadImages(){
	if(!sun)
		sun = new QPixmap("./data/gfx/sun16.png");
	if(!nightMap)
		nightMap = new QPixmap("./data/map/map-night.jpg");
}



void DaylightMaskGenerator::releaseImages(){
	safeDelete(sun);
	safeDelete(nightMap);
}



void DaylightMaskGenerator::precalc(UnixTime renderTime){
	std::tm *tmpTm = gmtime(&renderTime);
	utc = *tmpTm;

	//const float MAX_DECLINATION = 23.45f*(M_PI/180.0f);
	//declination = asin(sin(MAX_DECLINATION)*sin((360.0f/365.25f)*(utc.tm_yday-80.0f)));
	//declination = MAX_DECLINATION * sin(360.0f/365.0f*(284.0f+utc.tm_yday));
	const float TAU = 2.0f*M_PI*(utc.tm_yday-1.0f)/365.0f;
	declination =
		0.006918f - 0.399912*cos(TAU) + 0.070257f*sin(TAU)
		- 0.006758f*cos(2*TAU) + 0.000907f*sin(2*TAU)
		- 0.002697f*cos(3*TAU) + 0.001480f*sin(3*TAU);

	dayTime      = (utc.tm_sec+60.0f*utc.tm_min+60.0f*60.0f*utc.tm_hour)/(60.0f*60.0f*24.0f);
	sunLongitude = M_PI-2.0f*M_PI*dayTime;
}



void DaylightMaskGenerator::renderLightmap(QPainter& painter){
	painter.setPen(QColor(0, 0, 0, 127));
	for(int x=0; x<result->width(); x++){
		float longitude = MapLayer::pixelToLongitude(x, result->width()) - sunLongitude;
		int y = MapLayer::latitudeToPixel(-atan(cos(longitude)/tan(declination)), result->height());
		if(declination < 0)
			//painter.drawLine(x, 0, x, y);
			painter.drawPixmap(x, 0, 1, y, *resizedMap, x, 0, 1, y);
		else
			//painter.drawLine(x, y, x, result->height());
			painter.drawPixmap(x, y, 1, result->height(), *resizedMap, x, y, 1, result->height());
	}
}



QPixmap* DaylightMaskGenerator::render(UnixTime renderTime){
	precalc(renderTime);

	result->fill(QColor(Qt::transparent));
	QPainter painter(result);
	renderLightmap(painter);
	painter.drawPixmap(
		MapLayer::longitudeToPixel(sunLongitude, result->width())-sun->width()/2,
		MapLayer::latitudeToPixel(declination, result->height())-sun->height()/2,
		*sun);
	return result;
}
