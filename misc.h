/*
 * File:   misc.h
 * Author: peter
 *
 * Created on October 13, 2012, 5:53 PM
 */

#ifndef MISC_H
#define	MISC_H



#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <sstream>
#include <iostream>




template<class T>
void safeDelete(T* instance){
	if(instance){
		delete instance;
		instance = 0;
	}
}



class Satellite;
typedef std::vector<Satellite*> SatellitesVector;

typedef std::vector<std::string> StringVector;



static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}



template<typename T>
std::string toString(T t) {
    std::stringstream s;
    s << t;
    return s.str();
}



int toInt(const std::string& value);
long int toLong(const std::string& value);



#endif	/* MISC_H */
